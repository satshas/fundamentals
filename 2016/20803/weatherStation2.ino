#include "U8glib.h"

U8GLIB_ST7920_128X64 u8g(13, 11, 12, U8G_PIN_NONE);
//----------------------------DHT

#include <dht.h>

dht DHT;

#define DHT11_PIN 5


double temp=0;
double hum=0;
String temp_str;
String hum_str;
//-----------------------------------------Pressure


#include <SFE_BMP180.h>
#include <Wire.h>

// You will need to create an SFE_BMP180 object, here called "pressure":

SFE_BMP180 pressure;

double baseline; // baseline pressure

double pres=0;
double alt=0;
String pres_str;
String alt_str;

//-----------------------------------------Other
char  buff[20];

int decimalPlaces=2;

void setup(void) {
// assign default color value
if ( u8g.getMode() == U8G_MODE_R3G3B2 )
u8g.setColorIndex(255); // white
else if ( u8g.getMode() == U8G_MODE_GRAY2BIT )
u8g.setColorIndex(3); // max intensity
else if ( u8g.getMode() == U8G_MODE_BW )
u8g.setColorIndex(1); // pixel on


//--------------------DHT
Serial.begin(9600);
  Serial.println("DHT TEST PROGRAM ");
  Serial.print("LIBRARY VERSION: ");
  Serial.println(DHT_LIB_VERSION);
  Serial.println();
  Serial.println("Type,\tstatus,\tHumidity (%),\tTemperature (C)");
//------------------Pressure
  Serial.println("REBOOT");

  // Initialize the sensor (it is important to get calibration values stored on the device).

  if (pressure.begin())
    Serial.println("BMP180 init success");
  else
  {
    // Oops, something went wrong, this is usually a connection problem,
    // see the comments at the top of this sketch for the proper connections.

    Serial.println("BMP180 init fail (disconnected?)\n\n");
    while(1); // Pause forever.
  }

  // Get the baseline pressure:
  
  baseline = getPressure();
  
  Serial.print("baseline pressure: ");
  Serial.print(baseline);
  Serial.println(" mb");



}


//DRAW-------------------------------------------------------------
void draw(void) {
// graphic commands to redraw the complete screen should be placed here
u8g.setFont(u8g_font_04b_03b);
//u8g.setFont(u8g_font_osb21);

u8g.drawStr( 0, 12, "Welcome to my weather station");
//--------------------------------------------------
u8g.drawStr( 0, 22, "Temperature : ");
temp=DHT.temperature;
temp_str= doubleToString(DHT.temperature, decimalPlaces);
temp_str.toCharArray(buff,20);
u8g.drawStr( 57, 22, buff);
u8g.drawStr( 75, 22, " °C");
//--------------------------------------------------
u8g.drawStr( 0, 32, "Pressure : ");
pres_str= doubleToString(pres, decimalPlaces);
pres_str.toCharArray(buff,20);
u8g.drawStr( 44, 32, buff);
u8g.drawStr( 64, 32, " bar");
//--------------------------------------------------
u8g.drawStr( 0, 42, "Altitude : ");
alt_str= doubleToString(alt, decimalPlaces);
alt_str.toCharArray(buff,20);
u8g.drawStr( 40, 42, buff);
u8g.drawStr( 60, 42, " feet");
//--------------------------------------------------
u8g.drawStr( 0, 52, "Humidity : ");
hum=DHT.humidity;
hum_str= doubleToString(DHT.humidity, decimalPlaces);
hum_str.toCharArray(buff,20);
u8g.drawStr( 40, 52, buff);
u8g.drawStr( 65, 52, "%");


}
void DHT_sensor()
{
  // READ DATA
  Serial.print("DHT11, \t");
  int chk = DHT.read11(DHT11_PIN);
  switch (chk)
  {
    case DHTLIB_OK:  
                Serial.print("OK,\t"); 
                break;
    case DHTLIB_ERROR_CHECKSUM: 
                Serial.print("Checksum error,\t"); 
                break;
    case DHTLIB_ERROR_TIMEOUT: 
                Serial.print("Time out error,\t"); 
                break;
    case DHTLIB_ERROR_CONNECT:
        Serial.print("Connect error,\t");
        break;
    case DHTLIB_ERROR_ACK_L:
        Serial.print("Ack Low error,\t");
        break;
    case DHTLIB_ERROR_ACK_H:
        Serial.print("Ack High error,\t");
        break;
    default: 
                Serial.print("Unknown error,\t"); 
                break;
  }
  // DISPLAY DATA
  hum=DHT.humidity;
  Serial.print(hum, 1);
  Serial.print(",\t");
  temp=DHT.temperature;
  Serial.println(temp, 1);

  //delay(2000);
}
//------------------------------------Pressure

double getPressure()
{
  char status;
  double T,P,p0,a;

  // You must first get a temperature measurement to perform a pressure reading.
  
  // Start a temperature measurement:
  // If request is successful, the number of ms to wait is returned.
  // If request is unsuccessful, 0 is returned.

  status = pressure.startTemperature();
  if (status != 0)
  {
    // Wait for the measurement to complete:

    delay(status);

    // Retrieve the completed temperature measurement:
    // Note that the measurement is stored in the variable T.
    // Use '&T' to provide the address of T to the function.
    // Function returns 1 if successful, 0 if failure.

    status = pressure.getTemperature(T);
    if (status != 0)
    {
      // Start a pressure measurement:
      // The parameter is the oversampling setting, from 0 to 3 (highest res, longest wait).
      // If request is successful, the number of ms to wait is returned.
      // If request is unsuccessful, 0 is returned.

      status = pressure.startPressure(3);
      if (status != 0)
      {
        // Wait for the measurement to complete:
        delay(status);

        // Retrieve the completed pressure measurement:
        // Note that the measurement is stored in the variable P.
        // Use '&P' to provide the address of P.
        // Note also that the function requires the previous temperature measurement (T).
        // (If temperature is stable, you can do one temperature measurement for a number of pressure measurements.)
        // Function returns 1 if successful, 0 if failure.

        status = pressure.getPressure(P,T);
        if (status != 0)
        {
          return(P);
        }
        else Serial.println("error retrieving pressure measurement\n");
      }
      else Serial.println("error starting pressure measurement\n");
    }
    else Serial.println("error retrieving temperature measurement\n");
  }
  else Serial.println("error starting temperature measurement\n");
}





//----------------------------------------------------------------
String doubleToString(double input,int decimalPlaces){
if(decimalPlaces!=0){
String string = String((int)(input*pow(10,decimalPlaces)));
if(abs(input)<1){
if(input>0)
string = "0"+string;
else if(input<0)
string = string.substring(0,1)+"0"+string.substring(1);
}
return string.substring(0,string.length()-decimalPlaces)+"."+string.substring(string.length()-decimalPlaces);
}
else {
return String((int)input);
}
}

//----------------------------------------------------------------
void loop(void) {
//DHT------------------------------
  DHT_sensor();

//Pressure

double a,P=0;
  
  // Get a new pressure reading:

  P = getPressure();
  pres=P/1000;
      Serial.println(P,1);
      Serial.println(pres,1);


  // Show the relative altitude difference between
  // the new reading and the baseline reading:

  a = pressure.altitude(P,baseline);
  alt=a*3.28084; 
   Serial.println(a,1);
   Serial.println(alt,1);

  Serial.print("relative altitude: ");
  if (a >= 0.0) Serial.print(" "); // add a space for positive numbers
  Serial.print(a,1);
  Serial.print(" meters, ");
  if (a >= 0.0) Serial.print(" "); // add a space for positive numbers
  Serial.print(a*3.28084,0);
  Serial.println(" feet");

  
// picture loop
u8g.firstPage();
do {
draw();
} while( u8g.nextPage() );
DHT_sensor();
// rebuild the picture after some delay
delay(6000);



}
