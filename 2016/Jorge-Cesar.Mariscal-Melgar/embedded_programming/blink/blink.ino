
/*
//BLINK PROGRAM
//we want to use pin number 13, as it is connected to the yellow LED.
//first of all declare a pin variable to reference the pin. As a convention, use Capital letter.
int ledPin = 13;

//We need to assign names to variables that will remind us what they do.
boolean ledSwitch = false;


void setup() {
  // put your setup code here, to run once:
  // For the blink program, we need to set the pins. Orange colour illustrates build-in functions.
  // mode can be input or output.
  pinMode(ledPin, OUTPUT);

  //enable serial communication for debugging.
  Serial.begin(9600);
  
}

void loop() {
  // we want to turn on the light. We use digitalWrite function, it takes two inputs, the pin number and whether it is high or low.
  digitalWrite(ledPin, HIGH);
  // println will print a new line. This will send debug data to the serial feature of arduino. (top right on the arduino IDE, magnifier icon.
  Serial.println("ON");
  delay(1000);
  digitalWrite(ledPin, LOW);
  Serial.println("OFF");
  delay(1000);
}
*/

// BLINK PROGRAM 2

//we want to use pin number 13, as it is connected to the yellow LED.
//first of all declare a pin variable to reference the pin. As a convention, use Capital letter.
int ledPin = 13;

//We need to assign names to variables that will remind us what they do.
boolean ledSwitch = false;

//Declare a new variable.
int readValue = 100;

void setup() {
  // put your setup code here, to run once:
  // For the blink program, we need to set the pins. Orange colour illustrates build-in functions.
  // mode can be input or output.
  pinMode(ledPin, OUTPUT);

  //enable serial communication for debugging.
  Serial.begin(9600);
  
}

void loop() {
  // we can send up to 64bytes to the arduino. If serial.available is more than 0, then we know something is inside the buffer and we can read the value.
  if ( Serial.available() > 0){
    // We need to use parseint function (which can be found in the arduino reference)
    readValue = Serial.parseInt();
    // print in computer the value.
    if (readValue != 0) {
    Serial.println(readValue);
    }
  // to switch on the light when the value is 1.
  if (readValue == 1){
    digitalWrite(ledPin, HIGH);
  }
  else {
    if (readValue == 2) {
      digitalWrite(ledPin, LOW);
    }
  }
  }

  /*
  // we want to turn on the light. We use digitalWrite function, it takes two inputs, the pin number and whether it is high or low.
  digitalWrite(ledPin, HIGH);
  // println will print a new line. This will send debug data to the serial feature of arduino. (top right on the arduino IDE, magnifier icon.
  Serial.println("ON");
  delay(1000);
  digitalWrite(ledPin, LOW);
  Serial.println("OFF");
  delay(1000);
  */
}
