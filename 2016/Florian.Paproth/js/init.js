$(function() {
	
	$("<meta/>", {
		'http-equiv': "X-UA-Compatible",
		content: "IE=edge"
	}).appendTo("head");
	
	$("<meta/>", {
		name: "viewport",
		content: "width=device-width, initial-scale=1"
	}).appendTo("head");

	$("<link/>", {
		rel: "stylesheet",
		type: "text/css",
		href: "css/bootstrap.min.css"
	}).appendTo("head");
	
	$("<link/>", {
		rel: "stylesheet",
		type: "text/css",
		href: "css/bootstrap-theme.min.css"
	}).appendTo("head");
	
	$("<link/>", {
		rel: "stylesheet",
		type: "text/css",
		href: "css/sticky-footer.css",
	}).appendTo("head");
	
	// Latest compiled and minified JavaScript
	$("<script/>", {
		type: "text/javascript",
		src: "js/bootstrap.min.js"
	}).appendTo("head");
	
	// Load the navigation as first node into the body and mark the menue in which we are.
	// At first add a container to host a place for the navigation bar
	$('.container').prepend('<nav id="navigation" class="navbar navbar-default">');
	// Load the navigation via ajax and put it where the host container waits
	$('#navigation').load("navigation.html nav .container-fluid", function() {
		var current_path = window.location.pathname.split('/').pop();
		// Activate the right menu item that is selected
		$(".nav a[href='" + current_path + "']").parent().addClass('active');
	});
	
	$('body').append('<footer class="footer"><div class="container"><a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />Dieses Werk ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 4.0 International Lizenz</a>.</div></footer>');
});