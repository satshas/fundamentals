#include <Wire.h>
#include <Adafruit_BMP085_U.h>
#include <Adafruit_Sensor.h>
Adafruit_BMP085_Unified bmp = Adafruit_BMP085_Unified(10085);


long time1;
long time2;
long timeInterval;
int sensorPin;

void setup() {

  Serial.begin(9600);
  
  time1 = millis();
  time2 = millis();
  
  timeInterval = 3000;
  sensorPin = A0;

  pinMode(sensorPin, INPUT);

  }

void loop() {
  time2 = millis();
  

delay(1000);
  if((time2 - time1) >= timeInterval){
 
    
    Serial.println("sensor value: ");
    Serial.println(analogRead(sensorPin));
    time1 = time2;
  }

  }
