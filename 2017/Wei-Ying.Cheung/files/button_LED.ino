int ledPin = 2;
int buttonPin = 1;

void setup() {
  pinMode(ledPin,OUTPUT);
  pinMode(buttonPin,INPUT);
}

void loop() {
  if(digitalRead(buttonPin)==HIGH){
    digitalWrite(ledPin,HIGH);
  }else {
    digitalWrite(ledPin,LOW);
  }
}
