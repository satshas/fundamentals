#include <SoftwareSerial.h>
int sensorPin = A0;               // analog sensor attached to fabric sensor
int currentVal = 0;             // current value from fabric sensor
long start;
unsigned int count = 0;

int pumpPin = 8;
//int inflatevalvePin = 7;
//int exhaustValvePin = 3;
SoftwareSerial mySerial(10, 9);

//variable for calibration
int sensorValue=0;
int sensorMax= 0;
int thresh = 0;

void setup()                    
{
  pinMode(pumpPin, OUTPUT);      // sets the digital pin as output
  digitalWrite(pumpPin,LOW);
  pinMode(sensorPin, INPUT);  
  mySerial.begin(2400);
  //pinMode(inflatevalvePin, OUTPUT);
  //pinMode(exhaustValvePin, OUTPUT);
  //digitalWrite(exhaustValvePin,LOW);
  //digitalWrite(inletValvePin,LOW);
  //mySerial.println("program starting");

//calibration in the first 5 seconds
  while(millis()< 5000){
    sensorValue = analogRead(sensorPin);
   mySerial.print("sensor value = ");
   mySerial.println(sensorValue);
    // record the maximum value
    if( sensorValue> sensorMax){
      sensorMax= sensorValue;
    } 
  }
  thresh = sensorMax*0.99;

   mySerial.print("Threshold value = ");
   mySerial.println(thresh);
  
}

void loop()                     
{ 
 
  count = 0; // reset counter
  start = millis(); // take the start time
  while ((millis()-start) < 30000)   {      // do the following loop for 30000ms = 30secs
  
   currentVal = analogRead(sensorPin);
   mySerial.print("Current value = ");
   mySerial.println(currentVal);
   
   if (currentVal > thresh) {
       count ++;
       mySerial.print("Breaths = ");
       mySerial.println(count);
      }
      delay(500);
   }

 mySerial.println("Measure done");
 
  if (count > 10){
 
  count = 0; // reset couter
  start = millis(); // take the start time
  while ((millis()-start) < 30000)   {
    
    digitalWrite(pumpPin,HIGH); // make sure pump is on 
    //digitalWrite(inflatevalvePin, HIGH);
    //digitalWrite(exhaustValvePin,LOW); // turn exhaust valve LOW 
    delay(1000);
    //digitalWrite(inflatevalvePin,LOW); // turn pressure valve HIGH
    digitalWrite(pumpPin,LOW); // pump is off
    //digitalWrite(exhaustValvePin,HIGH); // turn exhaust valve HIGH 
    delay(4000);
  }

  }
  //digitalWrite(exhaustValvePin,LOW);
  //digitalWrite(inflatevalvePin,LOW);
  digitalWrite(pumpPin,LOW);
  
}
