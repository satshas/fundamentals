#include <SoftwareSerial.h>

int ledPin = PB0; // choose the pin for the LED
//int inPin = PB1;   // choose the input pin (for a pushbutton)
//int val = 0;     // variable for reading the pin status
//int countdown;
int pirPin = PB0; //declares PB1 as the pin for the PIR device
int pirState = 0;  //assume that the normal state is low, so no motion
SoftwareSerial mySerial (PB1, PB2);

void setup() {
  mySerial.begin(2400);
  mySerial.println("ATTiny 45 initiated");   // gives a start message
  pinMode(ledPin, OUTPUT);  // declare LED as output
  //pinMode(inPin, INPUT);    // declare pushbutton as input
  pinMode(pirPin, INPUT);   // declare PIR as Input device
}

void loop(){
  pirState = digitalRead(pirPin);   //reads pir input value

  if (pirState == HIGH) {
    digitalWrite(ledPin, HIGH);
    mySerial.println("Motion detected");
  }
  else {
    digitalWrite(ledPin, LOW);
    mySerial.println("No Motion detected");
  }
  delay(1000);
  /*val = digitalRead(inPin);  // read input value
  if (val == HIGH || countdown == 10) {         // check if the input is HIGH (button released)
    digitalWrite(ledPin, HIGH);  // turn LED ON
    mySerial.println("Button pressed");
    delay(1000);
  } else {
    digitalWrite(ledPin, LOW);  // turn LED OFF
    mySerial.println("Button not pressed");
/*
for (countdown = 0; countdown < 10; countdown++){

    mySerial.println(countdown);
    delay(1000);
  }
}
*/
}
