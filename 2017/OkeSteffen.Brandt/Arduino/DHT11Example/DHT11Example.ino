#include <dht.h>
#define SENSOR_PIN 3

dht humiditySensor;


void setup() {

 Serial.begin(115200);

 Serial.println("init");

}

void loop() {

humiditySensor.read11(SENSOR_PIN); //read the data

Serial.print("Humidity: ");
Serial.println(humiditySensor.humidity,2);
Serial.print("Temperature: ");
Serial.println(humiditySensor.temperature,2);

delay(1000);
}
