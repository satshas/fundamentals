#include <SoftwareSerial.h>  //include library

int ledPin = PB0; // choose the pin for the LED
//int val = 0;     // variable for reading the pin status

SoftwareSerial mySerial(PB1,PB2);  //RX and TX pins

void setup() {
  
  mySerial.begin(9600);  //start the software serial at 9600 baud rate
  pinMode(ledPin, OUTPUT);  // declare LED as output
}

void loop() {
 
  mySerial.println("Board_2 - Hello");  //send the message to the board#2
  
  delay(1000);
  
  if (mySerial.available() > 0) {  //if there is any message received (binary 1 > 0)
    
     mySerial.write(mySerial.read());  //print the message received
     digitalWrite(ledPin, HIGH);
     delay(1000);
     digitalWrite(ledPin, LOW);
     
    
   } 
}
