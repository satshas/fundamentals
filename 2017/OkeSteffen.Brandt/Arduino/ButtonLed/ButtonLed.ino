#include <SoftwareSerial.h>

int ledPin = PB0; // choose the pin for the LED
int inPin = PB1;   // choose the input pin (for a pushbutton)
int val = 0;     // variable for reading the pin status
int countdown;
SoftwareSerial mySerial (PB0, PB2);

void setup() {
  mySerial.begin(2400);
  mySerial.print("ATTiny 45 initiated");   // gives a start message
  pinMode(ledPin, OUTPUT);  // declare LED as output
  pinMode(inPin, INPUT);    // declare pushbutton as input
}

void loop(){
  val = digitalRead(inPin);  // read input value
  if (val == HIGH || countdown == 10) {         // check if the input is HIGH (button released)
    digitalWrite(ledPin, HIGH);  // turn LED ON
    mySerial.println("Button pressed");
    delay(1000);
  } else {
    digitalWrite(ledPin, LOW);  // turn LED OFF
    mySerial.println("Button not pressed");

for (countdown = 0; countdown < 10; countdown++){

    mySerial.println(countdown);
    delay(1000);
  }
}
}
