/* TinyTone for ATtiny85 */

// Notes
const int Note_C  = 239;
const int Note_CS = 225;
const int Note_D  = 213;
const int Note_DS = 201;
const int Note_E  = 190;
const int Note_F  = 179;
const int Note_FS = 169;
const int Note_G  = 159;
const int Note_GS = 150;
const int Note_A  = 142;
const int Note_AS = 134;
const int Note_B  = 127;

int speakerPin = PB1;  //declares PB1 as the pin for the speakers
int pirPin = PB2; //declares PB2 as the pin for the PIR device
int pirState = 0;  //assume that the normal state is low, so no motion
int ledPin = PB0;  // declares PB0 as the led pin

void setup()
{
  pinMode(speakerPin, OUTPUT);   // declare Speaker as output
  pinMode(pirPin, INPUT);    // declare PIR as input
  pinMode(ledPin, OUTPUT);    // declares ledPin as output
  delay(60000);              // 20 seconds delay before loop starts
}

void loop(){
  pirState = digitalRead(pirPin);   //reads if motions detected

  if (pirState == HIGH) {
    digitalWrite(ledPin, HIGH);
    playTune();
    delay(100);
    digitalWrite(ledPin, LOW);
    delay(400);
  }
}

void TinyTone(unsigned char divisor, unsigned char octave, unsigned long duration)
{
  TCCR1 = 0x90 | (8-octave); // for 1MHz clock
  // TCCR1 = 0x90 | (11-octave); // for 8MHz clock
  OCR1C = divisor-1;         // set the OCR
  delay(duration);
  TCCR1 = 0x90;              // stop the counter
}

// Play the alarm tone
void playTune(void)
{
 TinyTone(Note_G, 4, 500);
 TinyTone(Note_C, 4, 500);
}
