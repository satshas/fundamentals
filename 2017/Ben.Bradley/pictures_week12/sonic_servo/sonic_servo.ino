#include <SR04.h>
#include <SoftwareSerial.h>

SoftwareSerial mySerial (3, 4);

int servo_pin = 2;
int trigger_pin = 1;
int echo_pin = 0;
SR04 sr04 = SR04(echo_pin, trigger_pin);
long dist;
long sensitivity = 10; //distance below which the motor shall spin

void setup() {

  pinMode(servo_pin, OUTPUT);
  delay(1000);

}

void loop() {

  dist = sr04.Distance();
  mySerial.println(dist);
  if (dist < sensitivity){
    spinMotor(45);
    delay(1000);
    spinMotor(0);
    dist = 100; //stop it from repeating again immediately
    delay(5000);
  }

  delay(500);
  
}

void spinMotor(int degree){
  
  int pulseWidth;
  pulseWidth = (degree * 10) + 600; //calculate the delay needed for the servo to spin
  digitalWrite(servo_pin, HIGH);
  delayMicroseconds(pulseWidth);
  digitalWrite(servo_pin, LOW);
  
}
