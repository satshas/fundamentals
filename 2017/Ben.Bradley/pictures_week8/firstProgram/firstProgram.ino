#include <SoftwareSerial.h>

                int ledPin = 0;
                /*int buttonPin = 1;*/

                boolean light = false;

                int rxPin = 1;
                int txPin = 2;
            
                SoftwareSerial mySerial =  SoftwareSerial(rxPin, txPin);

                void setup() {
                    // put your setup code here, to run once:
                    pinMode(ledPin, OUTPUT);
                    pinMode(buttonPin, INPUT);

                    // define pin modes for tx, rx:
                    pinMode(rxPin, INPUT);
                    pinMode(txPin, OUTPUT);
                
                    // set the data rate for the SoftwareSerial port
                    mySerial.begin(4800);/**/
                }

                void loop() {
                    // put your main code here, to run repeatedly:
                    for(int i = 0; i < 100; i++){
                        mySerial.write("Hello");
                        if(!light){
                            digitalWrite(ledPin, HIGH);
                            light = true;
                        } else if (light){
                            digitalWrite(ledPin, LOW);
                            light = false;
                        }
                    }
                }
