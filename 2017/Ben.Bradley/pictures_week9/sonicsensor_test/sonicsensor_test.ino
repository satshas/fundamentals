#include <SoftwareSerial.h>

#include <SR04.h>


SoftwareSerial mySerial(4, 3); // RX, TX


#include "SR04.h"
#define TRIG_PIN 1
#define ECHO_PIN 0
SR04 sr04 = SR04(ECHO_PIN,TRIG_PIN);
long a;

void setup() {
   mySerial.begin(4800);
   delay(1000);
}

void loop() {
   a=sr04.Distance();
   mySerial.print(a);
   mySerial.println("cm");
   delay(1000);
}
