int servo = 0;
int pulseWidth;

void setup() {
  pinMode(servo, OUTPUT);
}

void spinMotor(int degree){
  pulseWidth = (degree * 10) + 600; //calculate the delay needed for the servo to spin
  digitalWrite(servo, HIGH);
  delayMicroseconds(pulseWidth);
  digitalWrite(servo, LOW);
}

void loop() {
  spinMotor(90); //spin motor

  delay(3000);

  spinMotor(0); //spin it back

  delay(3000);
}
