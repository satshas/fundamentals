#include <SoftwareSerial.h>  //include library

SoftwareSerial mySerial(10,11);  //RX and TX pins

void setup() {
  
  Serial.begin(4800);  //start the hardware serial at 4800 baud rate
  
  mySerial.begin(4800);  //start the software serial at 4800 baud rate
  
}

void loop() {
  
  delay(500);
  
  if (mySerial.available() > 0) {  //is there a message from the board available?
    
     Serial.println(mySerial.read());  //send the message to the computer
    
   } 
}
