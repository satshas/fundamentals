#include <SoftwareSerial.h>  //include library

SoftwareSerial mySerial(0,1);  //RX and TX pins

void setup() {
  
  mySerial.begin(4800);  //start the software serial at 4800 baud rate
  
}

void loop() {
  
  delay(5000);
  
  if (mySerial.available() > 0) {  //is there a message from the arduino available?
    
     mySerial.write("Board says bye");  //respond to the arduino
    
   } 
}
