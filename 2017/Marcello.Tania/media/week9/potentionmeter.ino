

// Input pin
int button =0; // select the pin for the button

// Output pin
int ledPin1 = 11;   // select the pin for the LED1
int ledPin2 = 12;   // select the pin for the LED2
int ledPin3 = 13;   // select the pin for the LED3

// Input Potentiometer
int sensorPin= A0; // select the pin for the potentiometer

double voltageValue;


void setup() {
  // put your setup code here, to run once:
   pinMode(ledPin1, OUTPUT);  // declare the ledPin as an OUTPUT
   pinMode(ledPin2, OUTPUT);  // declare the ledPin as an OUTPUT
   pinMode(ledPin3, OUTPUT);  // declare the ledPin as an OUTPUT
   pinMode(button, INPUT); // declare the button as an INPUT

  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);

}

void loop() {
  // read the input on analog pin 1:
  int sensorValue = analogRead(sensorPin);
  voltageValue = ((double)sensorValue)/1023*5;
  // print out the value you read:
  //Serial.print("SensorValue=");
  //Serial.println (sensorValue);
  Serial.print("voltageValue=");
  Serial.println (voltageValue);
  delay(100);
  
      if ((voltageValue<1.66))// 
      digitalWrite(ledPin1, HIGH);  // turn LED ON
      else
        digitalWrite(ledPin1, LOW);  // turn LED OFF
      
    if((voltageValue>1.66)&&(voltageValue<3.32))//
      digitalWrite(ledPin2, HIGH);  // turn LED ON
      else
        digitalWrite(ledPin2, LOW);  // turn LED OFF
      
    if ((voltageValue>3.32)&&(voltageValue<=5))
      digitalWrite(ledPin3, HIGH);  // turn LED ON
      else
        digitalWrite(ledPin3, LOW);  // turn LED OFF
  
}


