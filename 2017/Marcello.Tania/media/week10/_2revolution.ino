

// defines pins numbers
const int stepPin1 = 3; 
const int dirPin1 = 2; 


void setup() {
  // put your setup code here, to run once:
  pinMode(stepPin1,OUTPUT); 
  pinMode(dirPin1,OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
    digitalWrite(dirPin1,HIGH); // Enables the motor to move in a particular direction
  digitalWrite(stepPin1,LOW);  

  //two revolution
  for(int x = 0; x < 400; x++) {
    digitalWrite(stepPin1,HIGH); 
    delayMicroseconds(500); 
    digitalWrite(stepPin1,LOW); 
    delayMicroseconds(500); 
  }
  

}


