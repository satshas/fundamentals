
// defines pins numbers
// Input pin
int button =19; // select the pin for the button

// Output pin
const int stepPin1 = 3; 
const int dirPin1 = 2; 

const int stepPin2 = 5; 
const int dirPin2 = 4; 

const int stepPin3 = 6; 
const int dirPin3 = 7; 

// Input Values
boolean buttonState = false; // choose the input pin (for a pushbutton)
unsigned long now = millis(); // current (relative) time in msecs.

//State
int _state = 0; // starting with state 0: waiting for button to be pressed
unsigned long _startTime; // will be set in state 1
unsigned long _stopTime; // will be set in state 1
  
int _debounceTicks = 50;      // number of millisec that have to pass by before a click is assumed as safe.
int _clickTicks = 300;        // number of millisec that have to pass by before a click is detected.
int _pressTicks = 2000;       // number of millisec that have to pass by before a long button press is detected.


//led state to toogle
boolean stateLedYes;
boolean stateLedNo;

 
void setup() {
  // Sets the two pins as Outputs
  pinMode(stepPin1,OUTPUT); 
  pinMode(dirPin1,OUTPUT);

  pinMode(stepPin2,OUTPUT); 
  pinMode(dirPin2,OUTPUT);
  
  pinMode(stepPin3,OUTPUT); 
  pinMode(dirPin3,OUTPUT);

  
   pinMode(button, INPUT); // declare the button as an INPUT
   Serial.begin(9600);


  
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }


  Serial.println("Hallo!");


}



void loop() {
  
    // put your main code here, to run repeatedly:
  buttonState = digitalRead(button);  // read input value
  now= millis(); // update variable now
      //Starting (State 0)
    if (_state == 0) { // waiting for One pin being pressed.
      if ( buttonState == false) { //if button is pressed
         
         _state = 1; // step to state 1
         
         _startTime = now; // remember starting time
         
      //   Serial.println("state 0!");
      }

    }else if(_state == 1) { // waiting for One pin being released.
      // Serial.println("state 1!");
      // Serial.print("Start time:");
      // Serial.println(_startTime);
      // Serial.print("now:");
      // Serial.println(now);
       if ((buttonState == false) && (now > _startTime + _pressTicks)) { // /if button is pressed and pressed too long
         _state = 6; // step to state 6
         
       }else if(buttonState == true) { // if button is not pressed
          delay(_debounceTicks);
          _state = 2;
          _stopTime = now; // remember stopping time
       }else if((buttonState == true) &&  ((unsigned long)(now - _startTime) < _debounceTicks)){
          //Serial.println("State1 Debouncing");
        // button was released to fast means that this is debouncing
          _state =0;
       }else{
        //wait. Stay in this state      
       }


    }else if(_state == 6) { // waiting for One pin being release after long press.
        toogleNopeLED();
        //Serial.println("state 6!");
        if (buttonState == true) { // button is not pressed
           delay(_debounceTicks);
          _state = 0; // restart.
          //Serial.println("state 6!released");
        }
    }else if(_state ==2){
        if (now > _startTime + _clickTicks ) {
        // this was only a single short click
        //Serial.println("state 2!");
         
        click(); //toggle LED yes LED
         
        _state = 0; // restart.
       }else if ((buttonState == false)&& ((unsigned long)(now - _stopTime) > _debounceTicks) ){
          _startTime = now; // remember starting time
          _state =3;
          
         
       }
    }else if(_state ==3){
      if(  buttonState == true){// button release and this is double click
        
        doubleClick();
        //Serial.println("state 3!");
        //Serial.println("Double Click!");
         _startTime = now; // remember starting time
         delay(_debounceTicks); //debouncing delay
        _state = 0; // restart.
        
      }else if((buttonState == true) &&  ((unsigned long)(now - _startTime) < _debounceTicks)){
        // button was released to fast means that this is debouncing
          _state =0;
       }
        
    
        
       
    }

}

void up(){
  delay(1000);
  digitalWrite(dirPin1,HIGH); // Enables the motor to move in a particular direction
  digitalWrite(dirPin2,HIGH);
  digitalWrite(dirPin3,HIGH);
  digitalWrite(stepPin1,LOW);  
  digitalWrite(stepPin2,LOW); 
  digitalWrite(stepPin3,LOW); 
  
  for(int x = 0; x < 30; x++) {
    digitalWrite(stepPin1,HIGH); 
    digitalWrite(stepPin2,HIGH); 
    digitalWrite(stepPin3,HIGH); 
    delayMicroseconds(1000); 
    digitalWrite(stepPin1,LOW); 
    digitalWrite(stepPin2,LOW); 
    digitalWrite(stepPin3,LOW); 
    delayMicroseconds(1000); 
  }
}

void down(){
  delay(1000); //
  digitalWrite(dirPin1,LOW); //Changes the rotations direction
  digitalWrite(dirPin2,LOW); //Changes the rotations direction
  digitalWrite(dirPin3,LOW); //Changes the rotations direction
  digitalWrite(stepPin1,HIGH);  
  digitalWrite(stepPin2,HIGH);
  digitalWrite(stepPin3,HIGH);
  
  for(int x = 0; x < 30; x++) {
    digitalWrite(stepPin1,HIGH); 
    digitalWrite(stepPin2,HIGH); 
    digitalWrite(stepPin3,HIGH); 
    delayMicroseconds(1000); 
    digitalWrite(stepPin1,LOW); 
    digitalWrite(stepPin2,LOW);
    digitalWrite(stepPin3,LOW);
    delayMicroseconds(1000); 
  }
  delay(1000); //
}
void one(){
  delay(1000);
  
  digitalWrite(dirPin1,HIGH); // Enables the motor to move in a particular direction
  digitalWrite(stepPin1,LOW);  
  for(int x = 0; x < 30; x++) {
    digitalWrite(stepPin1,HIGH); 
    delayMicroseconds(1000); 
    digitalWrite(stepPin1,LOW); 
    delayMicroseconds(1000); 
  }
  
  delay(1000); //
  
    digitalWrite(dirPin1,LOW); //Changes the rotations direction
    digitalWrite(stepPin1,HIGH);  
  for(int x = 0; x < 30; x++) {
    digitalWrite(stepPin1,HIGH); 
    delayMicroseconds(1000); 
    digitalWrite(stepPin1,LOW); 
    delayMicroseconds(1000); 
  }
  delay(1000); //

    digitalWrite(dirPin2,HIGH); // Enables the motor to move in a particular direction
    digitalWrite(stepPin2,LOW);  
    for(int x = 0; x < 30; x++) {
    digitalWrite(stepPin2,HIGH); 
    delayMicroseconds(1000); 
    digitalWrite(stepPin2,LOW); 
    delayMicroseconds(1000); 
  }
  delay(1000); //
    digitalWrite(dirPin2,LOW); //Changes the rotations direction
    digitalWrite(stepPin2,HIGH);  
    for(int x = 0; x < 30; x++) {
    digitalWrite(stepPin2,HIGH); 
    delayMicroseconds(1000); 
    digitalWrite(stepPin2,LOW); 
    delayMicroseconds(1000); 
  }
  delay(1000); //


    digitalWrite(dirPin3,HIGH); // Enables the motor to move in a particular direction
    digitalWrite(stepPin3,LOW); 
     
    for(int x = 0; x < 30; x++) {
    digitalWrite(stepPin3,HIGH); 
    delayMicroseconds(1000); 
    digitalWrite(stepPin3,LOW); 
    delayMicroseconds(1000); 
    }
    
  delay(1000); //
  digitalWrite(dirPin3,LOW); //Changes the rotations direction
  digitalWrite(stepPin3,HIGH); 
   
    for(int x = 0; x < 30; x++) {
    digitalWrite(stepPin3,HIGH); 
    delayMicroseconds(1000); 
    digitalWrite(stepPin3,LOW); 
    delayMicroseconds(1000); 
  }
  delay(1000); //

}



void click(void){
  //PORTD ^= (1<<ledPinYes); //toogle led yes

  stateLedYes = !stateLedYes;
  delay(100);
  Serial.println("Click!");
  up();
  Serial.println("done!");
}

void doubleClick(void){

  stateLedNo = !stateLedNo;
  delay(100);
  Serial.println("Double Click!");
  down();
  Serial.println("done!");
}


void toogleNopeLED(void){

  Serial.println("long press");
  one();
  Serial.println("done!");
}

