// Input pin
int button =10; // select the pin for the button


// Input Values
boolean buttonState = false; // choose the input pin (for a pushbutton)
unsigned long now = millis(); // current (relative) time in msecs.

//State
int _state = 0; // starting with state 0: waiting for button to be pressed
unsigned long _startTime; // will be set in state 1
unsigned long _stopTime; // will be set in state 1
  
int _debounceTicks = 50;      // number of millisec that have to pass by before a click is assumed as safe.
int _clickTicks = 300;        // number of millisec that have to pass by before a click is detected.
int _pressTicks = 2000;       // number of millisec that have to pass by before a long button press is detected.


//led state to toogle
boolean stateLedYes;
boolean stateLedNo;

void setup() {
    // put your setup code here, to run once:

   pinMode(button, INPUT); // declare the button as an INPUT
   Serial.begin(9600);


  
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }


  Serial.println("Hallo from Arduino Uno!");


}

void loop() { // run over and over
    // put your main code here, to run repeatedly:
  buttonState = digitalRead(button);  // read input value
  now= millis(); // update variable now
      //Starting (State 0)
    if (_state == 0) { // waiting for One pin being pressed.
      if ( buttonState == false) { //if button is pressed
         
         _state = 1; // step to state 1
         
         _startTime = now; // remember starting time
         
      //   Serial.println("state 0!");
      }

    }else if(_state == 1) { // waiting for One pin being released.
      // Serial.println("state 1!");
      // Serial.print("Start time:");
      // Serial.println(_startTime);
      // Serial.print("now:");
      // Serial.println(now);
       if ((buttonState == false) && (now > _startTime + _pressTicks)) { // /if button is pressed and pressed too long
         _state = 6; // step to state 6
         
       }else if(buttonState == true) { // if button is not pressed
          delay(_debounceTicks);
          _state = 2;
          _stopTime = now; // remember stopping time
       }else if((buttonState == true) &&  ((unsigned long)(now - _startTime) < _debounceTicks)){
          //Serial.println("State1 Debouncing");
        // button was released to fast means that this is debouncing
          _state =0;
       }else{
        //wait. Stay in this state      
       }


    }else if(_state == 6) { // waiting for One pin being release after long press.
        toogleNopeLED();
        //Serial.println("state 6!");
        if (buttonState == true) { // button is not pressed
           delay(_debounceTicks);
          _state = 0; // restart.
          //Serial.println("state 6!released");
        }
    }else if(_state ==2){
        if (now > _startTime + _clickTicks ) {
        // this was only a single short click
        //Serial.println("state 2!");
         
        click(); //toggle LED yes LED
         
        _state = 0; // restart.
       }else if ((buttonState == false)&& ((unsigned long)(now - _stopTime) > _debounceTicks) ){
          _startTime = now; // remember starting time
          _state =3;
          
         
       }
    }else if(_state ==3){
      if(  buttonState == true){// button release and this is double click
        
        doubleClick();
        //Serial.println("state 3!");
        //Serial.println("Double Click!");
         _startTime = now; // remember starting time
         delay(_debounceTicks); //debouncing delay
        _state = 0; // restart.
        
      }else if((buttonState == true) &&  ((unsigned long)(now - _startTime) < _debounceTicks)){
        // button was released to fast means that this is debouncing
          _state =0;
       }
        
    
        
       
    }

}


void click(void){
  //PORTD ^= (1<<ledPinYes); //toogle led yes

  stateLedYes = !stateLedYes;
  delay(100);
  Serial.println("Click!");
}

void doubleClick(void){

  stateLedNo = !stateLedNo;
  delay(100);
  Serial.println("Double Click!");
}


void toogleNopeLED(void){

  Serial.println("long press");
  
}
