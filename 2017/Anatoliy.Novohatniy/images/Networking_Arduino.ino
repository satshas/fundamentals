#include <SoftwareSerial.h>

SoftwareSerial mySerial(10,11);  // initialize the pin RX, TX of Arduino

void setup() {

// Open serial communications and wait for port to open:  

Serial.begin(4800);
mySerial.begin(4800);  


Serial.println("Arduino started"); // print only one time to show up that Arduino is connected

}

void loop() {  // run over and over

  if (mySerial.available()) {
    Serial.write(mySerial.read());
  }

  if (Serial.available())
  mySerial.write(Serial.read());

}
