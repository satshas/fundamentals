#include <SoftwareSerial.h>

SoftwareSerial mySerial(1, 0); //RX, TX

void setup() {

// Open serial communications and wait for port to open: 
  
mySerial.begin(4800);  
mySerial.println("Yo");

}

void loop() {  // run over and over 

  if (mySerial.available()) {       // if my board has active connection 
  mySerial.println("Work");         // print "Work" all the time to show that connection is established

    }
}
