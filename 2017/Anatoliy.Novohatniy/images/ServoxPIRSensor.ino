#include <Servo.h>

Servo myservo;      // creates a servo object

int pos = 1;        //variable to store servo position
int state; 
int majorLED = A3;
//amount of time we give the sensor to calibrate(10-60 secs according to the datasheet)

int calibrationTime = 15;

boolean illumination;
boolean PIRstatus; 
boolean PreviousState = LOW;

const byte pirPin = 12;           //digital pin connected to the PIR's output
const byte pirPos = 13;           //connects to the PIR's 5V pin

void spinRight(){
    while(pos <= 115){
    pos++;                             // goes from 1 to 115 degrees
    {                                  // in steps of one degree
      myservo.write(pos);              // tells servo to go to position in variable "pos"
      delay(15);                        //waits for the servo to reach the position
    }
  }
}
    
void spinLeft(){    
  while(pos >= 1){    //goes from 115 to 1 degrees
    pos--;
    {                               
      myservo.write(pos);                  //to make the servo go faster, decrease the time in delays for
      delay(15);                           //to make it go slower, increase the number.
    }
  }
}

void spinStop()
{
  illumination = !illumination;                // toggle switch state
  state = 0;                                // reset distance to stop accidental operation
  if (illumination == false)                   // make sure LED is HIGH for open and LOW for closed
  {
    digitalWrite(majorLED, LOW);
  }
  else
  {
    digitalWrite(majorLED, HIGH);
  }
  delay(1000);                                // prevent operation too quickly
}
  

void setup(){
  
  myservo.attach(8);    //attaches servo to pin 4
  myservo.write(1);
  Serial.begin(9600);    //begins serial communication
  pinMode(pirPin, INPUT);
  pinMode(pirPos, OUTPUT);
  digitalWrite(pirPos, HIGH);

  //give the sensor time to calibrate
  Serial.println("calibrating sensor ");
  for(int i = 0; i < calibrationTime; i++){
    Serial.print(calibrationTime - i);
    Serial.print("-");
    delay(1000);
  }
  Serial.println();
  Serial.println("done");
 
  //while making this Instructable, I had some issues with the PIR's output
  //going HIGH immediately after calibrating
  //this waits until the PIR's output is low before ending setup
  while (digitalRead(pirPin) == HIGH) {
    delay(500);
    Serial.print(".");     
  }
  Serial.println("SENSOR ACTIVE");
}

void loop(){

    Serial.println(pirPos);

  /*turns servo from 0 to 180 degrees and back
    it does this by increasing the variable "pos" by 1 every 5 milliseconds until it hits 180
    and setting the servo's position in degrees to "pos" every 5 milliseconds
    it then does it in reverse to have it go back
    to learn more about this, google "for loops"
    to change the amount of degrees the servo turns, change the number 180 to the number of degrees you want it to turn
    **/
  PIRstatus = digitalRead(pirPin);
  if (PIRstatus != PreviousState){
      PreviousState = PIRstatus;
  if(PIRstatus == HIGH){  //if the PIR output is HIGH, turn servo
     if (myservo.read() == 1){
      spinRight();
     
  }else{
      spinLeft();
  }
  }
  }
}
