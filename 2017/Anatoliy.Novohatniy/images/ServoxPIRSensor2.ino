#include <Servo.h>

Servo myservo;

int pos = 0;               // variable to store servo position
int state = 0;
int calibrationTime = 12;  // amount of time we give the sensor to calibrate(10-60 secs according to the datasheet)
long unsigned int lowIn;   // the time when the sensor outputs a low impulse     

// the amount of milliseconds the sensor has to be low
// before we assume all motion has stopped
long unsigned int pause = 5000; 

boolean lockLow = true;
boolean takeLowTime; 

int pirPin = 12;            // digital pin connected to the PIR's output
int pirPos = 13;            // connects to the PIR's 5V pin

void setup(){
  myservo.attach(8);    //servo to 8
  Serial.begin(9600);  
  pinMode(pirPin, INPUT);
  pinMode(pirPos, OUTPUT);
  digitalWrite(pirPos, HIGH);

  for(int i = 0; i < calibrationTime; i++){
    Serial.print(calibrationTime - i);
    Serial.print("-");
    delay(1000);
  }
  Serial.println();
  Serial.println("done");
 
{
    delay(500);
    Serial.print(".");     
  }
  Serial.print("SENSOR ACTIVE");


for(pos = 0; pos < 180; pos += 1)  
    {                                                
      myservo.write(pos);                 
      delay(5);                               
    }
    for(pos = 180; pos>=1; pos-=1) 
    {                               
      myservo.write(pos);  
      delay(10); 
}
}
void loop(){

  if(digitalRead(pirPin) == HIGH){    
    
    if(lockLow){ 
      //makes sure we wait for a transition to LOW before further output is made
      lockLow = false;           
      Serial.println("---");
      Serial.print("motion detected at ");
      Serial.print(millis()/1000);
      Serial.println(" sec");
      delay(50);
    }        
    takeLowTime = true;
 
  if(digitalRead(pirPin) == LOW){      

    if(takeLowTime){
      lowIn = millis();             //save the time of the transition from HIGH to LOW
      takeLowTime = false;    //make sure this is only done at the start of a LOW phase
    }
   
    //if the sensor is low for more than the given pause,
    //we can assume the motion has stopped
    if(!lockLow && millis() - lowIn > pause){
      //makes sure this block of code is only executed again after
      //a new motion sequence has been detected
      lockLow = true;                       
      Serial.print("motion ended at "); //output
      Serial.print((millis() - pause)/1000);
      Serial.println(" sec");
      delay(50);
    }
  }
}
}

