
int ledPin = 0;       // the number of the LED pin
int buttonPin = 1;    // the number of the pushbutton pin
int brightness = 0;   // stands for the value that is output in the PWM. The number 0 is only an arbitrary starting value
int fadesteps = 5;    // determines the speed of the fade
int buttonState = 0;  // variable for reading the pushbutton status

void setup()

{
  
  pinMode(ledPin, OUTPUT);    // ledPin is an output, 0 because of the arrangement of the microcontroller (Attiny45)
  pinMode(buttonPin, INPUT);  // for button as well but it's an input

}

void loop()
{

  buttonState = digitalRead(buttonPin);      // read the state of the pushbutton value 
  
  if (buttonState == HIGH) {                 // check if the pushbutton is pressed. If it is, the buttonState is HIGH
  analogWrite(ledPin, brightness);           // turn LED on
  brightness = brightness + fadesteps;       // the value "brightness" for the fadesteps is now added to the previous brightness. In this case: brightness = 0 + 5. The new value for "brightness" is therefore no longer 0 but 5. As soon as the loop part has passed once, it repeats itself. Then the value for the brightness is 10. In the next pass 15 etc.
  delay(25);                                 // the brightness remains on 25ms (milliseconds) and then LED gehts darker again (slow dim), but decreasing this value also makes pulsation faster
  if (brightness == 0 || brightness == 255)  // when the brightness reaches the value 0 or 255, the value for the "fadesteps" changes from positive to negative or negative to positive
  {
  
  fadesteps = -fadesteps;                    // the brightness decreases here
  
} else {
    
    analogWrite(ledPin, LOW);          // turn LED off
    }

  }
}
