#include <Servo.h>

void ServoControl();      // function is written here
void ServoControlReset(); // function is written here

Servo myservo;          // create servo object

bool toggle = false;    // used as a one shot
int calibrationTime = 15;

int pos = 0;            // initial servo position
int PIRpin = 7;         // PIR Sensor on pin 5
int distance = 0;       // variable that will tell if there is an obstacle or not
int mainLED = 5;        // control for the main lights
int closed = 1;         // Servo closed angle
int open = 145;         // Servo open angle

void setup()
{
  myservo.attach(9);      // attaches servo to pin 9
  myservo.write(1);
  Serial.begin(9600);     // begins serial communication
  pinMode(PIRpin, INPUT);
  pinMode(mainLED, OUTPUT);

  // give the sensor time to calibrate
  Serial.println("calibrating sensor ");
  for(int i = 0; i < calibrationTime; i++){
    Serial.print(calibrationTime - i);
    Serial.print("-");
    delay(1000);
  }
  Serial.println();
  Serial.println("done");
 
  //going HIGH immediately after calibrating
  //this waits until the PIR's output is low before ending setup
  while (digitalRead(PIRpin) == HIGH) {
    delay(500);
    Serial.print(".");     
  }
  Serial.println("SENSOR ACTIVE");
}

void loop()
{
    
   Serial.println(digitalRead(PIRpin));               // read data from pin of PIR Sensor

  if (digitalRead(PIRpin) && toggle == false)         // open up lamp and turn on the lights
  {
    for(pos = closed; pos <= open; pos++)             // goes from 1 degree to 145 degrees
    {                                                 // in steps of 1 degree
      ServoControl(pos);                              // Set Servo Position and Control LED Brightness
    }
      ServoControlReset();                            // when finished, reset variables etc.
      
      delay(10000);
  }else
  if (digitalRead(PIRpin) && toggle == true)          // turn off lamp and close
  {
    for(pos = open; pos > closed; pos--)              // goes from 145 degrees to 1 degree
    {
      ServoControl(pos);                              // Set Servo Position and Control LED Brightness
    }
    ServoControlReset();                              // when finished, reset variables etc.
    
      delay(10000);
  }
}

// Servo position control and LED brightness
void ServoControl(int ServoPosition)
{
  int LEDmap = ServoPosition;
  LEDmap = map(LEDmap, closed, open, 255, 0);  // Map servo position to LED brightness
  myservo.write(ServoPosition);                // tell servo to go to position in variable 'pos'
  delay(15);                                   // waits 25ms for the servo to reach the position
  analogWrite(mainLED, LEDmap);                //ramp light on using PWM Pin
}

// After servo has reached final position, reset variables and wait.
void ServoControlReset(void)
{
  toggle = !toggle;                           // toggle switch state
  distance = 0;                               // reest distance to stop accidental operation
  if (toggle == false)                        // make sure LED is High for open and low for closed
  {
      digitalWrite(mainLED, LOW);
  }
  else
  {
     digitalWrite(mainLED, HIGH);
  }
  delay(1000);                                // prevent operation too quickly
}
