#include <SoftwareSerial.h>

SoftwareSerial mySerial(1, 0);

int senPin = A1;       // sensor is connected to pin 2 (A1) in my case
int sensorValue = 0;  // if not pressed, always 0

void setup() 
{
  // Start serial at 9600 baud
  mySerial.begin(9600);
  pinMode(senPin, INPUT);       
}

    void loop() 
 {

  // Read the input on analog pin 2:
  sensorValue = analogRead(senPin);
  float pressure = sensorValue * ( 5.0 / 1023.0 );  

   mySerial.print("Pressure: ");
   mySerial.println(pressure); 

  //Delay between pressing
   delay(100);
  }
 
