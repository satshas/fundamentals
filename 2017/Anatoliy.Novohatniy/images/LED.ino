void setup() {
  // put your setup code here, to run once
  // initialize digital pin 13 as an output

  pinMode(2, OUTPUT);

}

void loop() {
  // the loop function runs over and over again forever

  digitalWrite(2, HIGH);    // turn the LED on (HIGH is the voltage level)
  delay(1000);              // wait for a second
  digitalWrite(2, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);              // wait for a second
}
