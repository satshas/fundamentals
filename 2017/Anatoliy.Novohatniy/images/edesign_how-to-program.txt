 1 - connect the arduino uno to the pc
 2 - select the right port and the arduino uno board under tools
 3 - under file->examples find and open the arduino as isp sketch
 4 - upload the sketch to the arduino
 5 - disconnect the arduino from the pc
 6 - connect the hello board with the arduino (check the connection schema)
 7 - triple check the connections
 8 - connect the arduino to the pc
 9 - select the right board/processor/frequency -> attiny25/45/85, attiny45, internal 8mhz
 10 - under tools select the arduino as isp programmer
 11 - double check all the paramters
 12 - click to tools-> burn bootloader
 13 - write your own program
 14 - to program the board do sketch->upload using programmer
