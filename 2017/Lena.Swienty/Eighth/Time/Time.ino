#include <SoftwareSerial.h>    
SoftwareSerial mySerial(0,2);
long time;                      //I created a variable of type long with the name 'time'

void setup() {
 mySerial.begin(4800);
 pinMode(PB0, OUTPUT);
 pinMode(PB1, INPUT); 
}
void loop() {
   time = millis();                     //millis() is a function which returns the number of the milliseconds since starting the current programm
   if(digitalRead(PB1) == HIGH){
    digitalWrite(PB0, LOW);
    while(digitalRead(PB1) == HIGH){    //a loop which checks if the button is pressed..
    mySerial.println(time);             //prints the milliseconds in the serial monitor..
    delay(1000);                        //with a delay of one second
    } 
    }else if(digitalRead(PB1) == LOW){  
    digitalWrite(PB0, HIGH);
   }
  
}
