#include <SoftwareSerial.h>   //to use the Serial
SoftwareSerial mySerial(0,2); //these are TX and RX

void setup() {
 mySerial.begin(4800);        //the delay until the serial begins
 pinMode(PB0, OUTPUT);        //the led
 pinMode(PB1, INPUT);         //the button
 
}
void loop() {
  if(digitalRead(PB1) == HIGH){       //if the button is pressed..
    digitalWrite(PB0, LOW);           //do not let the led shine
    mySerial.println("1");            //print while button is pressed 1 in the Serial Monitor
  }else if(digitalRead(PB1) == LOW){  //if the button is not pressed..
    digitalWrite(PB0, HIGH);          //let the led shine
    mySerial.println("0");            //print while button is not pressed 0 in the Serial Monitor
  }
}
