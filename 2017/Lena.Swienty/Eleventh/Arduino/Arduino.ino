//Arduino

#include <SoftwareSerial.h>

SoftwareSerial mySerial(11,12);

void setup() {
  Serial.begin(4800);
  mySerial.begin(4800);
  Serial.write("HelloBoard");
}

void loop() {

  mySerial.println("Hello Board");
  delay(1000);
  while(mySerial.available()){
    Serial.write(mySerial.read());
  }
}
