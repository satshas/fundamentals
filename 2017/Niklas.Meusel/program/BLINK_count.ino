const int buttonPin = 1;     // the number of the pushbutton pin
const int ledPin =  0;      // the number of the LED pin

int buttonState = 0;         // variable for reading the pushbutton status

void setup() {
  // initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);
  // initialize the pushbutton pin as an input:
  pinMode(buttonPin, INPUT);
}

void loop() {
  // read the state of the pushbutton value:
  buttonState = digitalRead(buttonPin);

  // check if the pushbutton is pressed. If it is, the buttonState is HIGH:
  if (buttonState == HIGH) {

    for(int count=1; count <= 5; count++){
      
      for(int i= 1; i <= count; i++){ 
        digitalWrite(ledPin, HIGH);
        delay(100);
        digitalWrite(ledPin, LOW);
        delay(500);   
      }
      delay(1000);
      }
       
  } else {
    // turn LED off:
    digitalWrite(ledPin, LOW);
  }
}
