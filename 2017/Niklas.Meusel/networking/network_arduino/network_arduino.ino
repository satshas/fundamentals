#include <SoftwareSerial.h>

SoftwareSerial mySerial(10, 11); // RX, TX

void setup() { 
  Serial.begin(4800);
  while (!Serial) {
    ; 
  }
  Serial.println("Started!");
  mySerial.begin(4800);
}

void loop() { 
  if (mySerial.available()) {
    Serial.write(mySerial.read());
  }
  if (Serial.available()) {
    mySerial.write(Serial.read());
  }
}
