#include <MozziGuts.h> //Include the Mozzi library to be used
#include <Oscil.h> //Include the oscillator template for producing sound
#include <tables/cos2048_int8.h> //Read sin wave for use with oscillator
#include <Smooth.h>
#include <AutoMap.h>
#include <EventDelay.h>

const int MIN_CARRIER_FREQ = 22;
const int MAX_CARRIER_FREQ = 440;

const int MIN_INTENSITY = 700;
const int MAX_INTENSITY = 10;

const int MIN_MOD_SPEED = 10000;
const int MAX_MOD_SPEED = 1;

AutoMap kMapCarrierFreq(0,1023,MIN_CARRIER_FREQ,MAX_CARRIER_FREQ);
AutoMap kMapIntensity(0,1023,MIN_INTENSITY,MAX_INTENSITY);
AutoMap kMapModSpeed(0,1023,MIN_MOD_SPEED,MAX_MOD_SPEED);


const int KNOB_PIN = 5;
const int LDR1_PIN = 4;
const int LDR2_PIN = 1;

Oscil<COS2048_NUM_CELLS, AUDIO_RATE> aCarrier(COS2048_DATA);
Oscil<COS2048_NUM_CELLS, AUDIO_RATE> aModulator(COS2048_DATA);
Oscil<COS2048_NUM_CELLS, CONTROL_RATE> kIntensityMod(COS2048_DATA);

int mod_ratio = 5;
long fm_intensity;

float smoothness = 0.95f;

Smooth <long> aSmoothIntensity(smoothness);

void setup(){
 
  Serial.begin(115200);
  startMozzi();
  
}

void updateControl(){

  int knob_value = mozziAnalogRead(KNOB_PIN);
  int carrier_freq = kMapCarrierFreq(knob_value);
  int mod_freq = carrier_freq * mod_ratio;

  aCarrier.setFreq(carrier_freq);
  aModulator.setFreq(mod_freq);

  int LDR1_value = mozziAnalogRead(LDR1_PIN);

  int LDR1_calibrated = kMapIntensity(LDR1_value);

  fm_intensity = ((long)LDR1_calibrated * (kIntensityMod.next()+128))>>8;

  int LDR2_value = mozziAnalogRead(LDR2_PIN);

  float mod_speed = (float)kMapModSpeed(LDR2_value)/1000;
  kIntensityMod.setFreq(mod_speed);
}

int updateAudio(){

  long modulation = aSmoothIntensity.next(fm_intensity) * aModulator.next();
  return aCarrier.phMod(modulation);
}

void loop(){
  audioHook();
    
}


