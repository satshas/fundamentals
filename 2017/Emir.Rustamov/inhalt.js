


var topicList = [

    {
       topic: "Project Management",
       body:[
           {
               text:"First of all we have to download a Git. All our projects will be posted in git. \n Git is a very usefull collaboration tool on one projekt with you team or you have tu make your projekt public. \n The Installation is very simple. In Windows you have an .exe file that you can download from here: https://git-scm.com/downloads \n In Linux you have to write following commands in to your terminal: \n sudo apt-get update \n sudo apt-get install git\n And it is Ready to use! ",
               bild:"shots/gitdownload.png"
           },
           {
                text:"I am working with Windows 10, and this is a Git Bash which understands a Linux terminal commands.",
                bild:"shots/linuxcommands.png"
            },
            {
                text:"You can choose a folder where you like to have your Projekt like this: \n cd path/to/desired/folder \n I wished to have it in standard directory c/Users/your_usermane. \n You can also check your directory with 'pwd' command",
                bild:"shots/gitbash.png"
            },
            {
                text:"So now we can start to wort with our Git, first of all we have to define our username.\n Do do this you need the followin command: \n git config --global user.name 'Your_Name Your_Surname'",
                bild:"shots/gitusername.png"
            },
            {
                text:"Then we have to set our e-mail adress, with: \n git config --global user.mail 'your_e-mail' ",
                bild:"shots/gitemail.png"
            },
            {
                text:"After the username and usermail set we have to generate a ssh key that we will connect with the Git account.\n To generate the Key we need to use the following command: \n ssh-keygen -t rsa -C 'your_e-mail'\n and then press enter some times. I have only written a code on the Image below(without enter)",
                bild:"shots/gitsshkey.png"
            },
            {
                text:"Here is the ssh key File, we have to open it with text editor and copy the content.",
                bild:"shots/gitsshkey1.png"
            },
            {
                text:"After that, we need to log-in at http://gitlab.hsrw.org, open the 'Burger' at the left-top of the page, open the 'Profile Setting' menu,\n choose 'SSH Key' at the top bar and paste the ssh key.",
                bild:"shots/gitsshkey2.png"
            },
            {
                text:"Than we have to go to the main page of our project, change the protocol to http and copy a changed text.",
                bild:"shots/http.png"
            },
            {
                text:"Now we a ready to download you repository our local machiene with Git, to do this we have to type the following command: \n git clone an_paste_the_what_you_have_copied_on_the_previous_step",
                bild:"shots/reptolocal.png"
            },
            {
                text:"Now can create an index.html to work on it with the command: \n touch index.html",
                bild:"shots/index.png"
            },
            {
                text:"Now we have finished to work with index.html and other files and want them upload to the repository.\n We have to do it in three steps, first add app file you wont to upload: \n git add index.html \n or all files: \n git add * ",
                bild:"shots/add.png"
            },
            {
                text:"Then we have to describe the changes in one sentence with: \n git commit -m 'Write_the_changes_here'",
                bild:"shots/commit.png"
            },
            {
                text:"We can also check a status of the files: \n git status\n You can see a good example of untracked files, all files are untracked withaut an 'add'.",
                bild:"shots/status.png"
            },

            {
                text:"The last step is upload or push to git with: \n git push \n And remember you may turn of your Firewall if you want to work properly with Git.",
                bild:"shots/push.png"
            },
            {
                text:"This is how my html code looks like. I have used Materialize Framework",
                bild:""
            },
            {
                text:"<pre class='prettyprint lang-html'>\n  <head>\n    <link rel='stylesheet' href='indexstyle.css'>\n    <!--Import Google Icon Font-->\n    <link href='https://fonts.googleapis.com/icon?family=Material+Icons' rel='stylesheet'>\n    <!--Import materialize.css-->\n    <link type='text/css' rel='stylesheet' href='css/materialize.min.css'  media='screen,projection'/>\n    <!--Let browser know website is optimized for mobile-->\n    <meta name='viewport' content='width=device-width, initial-scale=1.0'/>\n  </head> \n  <body>\n    <!--Import jQuery before materialize.js-->\n    <script type='text/javascript' src='https://code.jquery.com/jquery-3.2.1.min.js'></script>\n    <script type='text/javascript' src='js/materialize.min.js'></script>\n    <script src='inhalt.js'></script>\n    <script src='script.js'></script>\n    <!-- The Nav Header -->\n    <div class ='container'>\n    <!-- Dropdown Structure -->\n    <ul id='dropdown1' class='dropdown-content'>\n            <li><a class='clicked'>01 Project Management</a></li>\n            <li class='divider'></li>\n            <li><a class='clicked'>02 2D und 3D Design</a></li>\n            <li><a class='clicked'>03 Laser cutting</a></li>\n            <li><a class='clicked'>04 3D Printing</a></li>\n            <li><a class='clicked'>05 CNC Machining</a></li>\n            <li><a class='clicked'>06 Electronics Production</a></li>\n            <li><a class='clicked'>07 Electronics Dedign</a></li>\n            <li><a class='clicked'>08 Embedded Programming</a></li>\n            <li><a class='clicked'>09 Input Device</a></li>\n            <li><a class='clicked'>10 Output Device</a></li>\n            <li><a class='clicked'>11 Networking</a></li>\n            <li><a class='clicked'>12 Final Project</a></li>\n          </ul>\n      <nav id='nav0' class='light-blue darken-4'>\n        <div id='nav1' class='nav-wrapper '>\n          <img id='fabLabLogo' class='z-depth-5' src='FabLabLogo.jpg'>\n          <ul class='right hide-on-med-and-down'>\n            <li><a onclick='locationReload()'>About</a></li>\n            <!-- Dropdown Trigger -->\n            <li><a  id='breiter' class='dropdown-button' href='#!' data-activates='dropdown1'>Lessons<i class='material-icons right'>arrow_drop_down</i></a></li>\n          </ul>\n        </div>\n      </nav>\n      <span id='header1'>\n            Emir Rustamov\n      </span>\n      <div id='inhalt' class='row'>\n      <img id ='foto' src='Foto.jpg'>\n      <h4>\n          I'm 24 years old, live in Germany and study E-Government.\n          \n\n          Always asking dumb questions ;)\n          \n\n          ---------------------------------------------------------\n          \n\n          Contact Details:\n          \n\n          email: rustamov.emir@gmail.com\n        </h4>\n    </div>\n    </div>\n  </body>\n</html> </code>   </pre>",
                bild:""
            },
           {
                text:"This is how my JS cools like, my WebPage is dynamic, so i don't need to create a new static page if we need to create a new HTML for a documentation about new topic. \n I have a file names inhalt.js, it hast an array with Data which I call if someone chooses another topic. \n But unfortunately the Dynamic WebPage has some issue with Prettify. If i call the Data after the page is loaded, it doesn't work properly.\n So I have to load Prettify JQ dynamic too.",
                bild:""
            },
           {
                text:"<pre class='prettyprint lang-java'> /* Made By Emir Rustamov */\n    $( document ).ready(function() {\n        $('.clicked').click(function(){\n            var inhalt = $(this).text();\n            var compare = parseInt(inhalt[0].toString()+inhalt[1].toString()) -1;\n            console.log(compare);\n            if(topicList[compare].body.length == 0){\n                var inhalt = document.getElementById('inhalt');\n                inhalt.innerHTML = '';\n                document.getElementById('header1').innerHTML = 'Gratulations, you found a secret Page. Try to scroll.';\n                inhalt.style.height = '1200px';\n            }else{                //ausgabe der Inhalte\n                var topic = document.getElementById('header1');\n                topic.innerHTML = topicList[compare].topic;\n                var inhalt = document.getElementById('inhalt');\n                inhalt.innerHTML = '';\n                topicList[compare].body.forEach(function(element) {\n                    var divs = document.createElement('div');\n                    divs.setAttribute('class','col s3 m6 l12');\n                    if(element.text[29] == 'j' && element.text[30] == 'a' && element.text[31] == 'v' && element.text[32] == 'a'){\n                        $(divs).append(element.text);\n                        $.loadScript('url_to_someScript.js', function(){\n                            console.log('Prettify Loaded');\n                        });\n                    }else if(element.text[29] == 'h' && element.text[30] == 't' && element.text[31] == 'm' && element.text[32] == 'l'){\n                        divs.innerText=element.text;\n                        $.loadScript('url_to_someScript.js', function(){\n                            console.log('Prettify Loaded');\n                        });\n                    }else if(element.text[2] == 'c' && element.text[3] == 's' && element.text[4] == 's'){\n                        divs.innerText=element.text;\n                        divs.style.border='1px solid black';\n                        $.loadScript('url_to_someScript.js', function(){\n                            console.log('Prettify Loaded');\n                        });\n                    }else{\n                        divs.style.marginTop = '40px';\n                        divs.innerText=element.text;\n                    }\n                    var images = document.createElement('img');\n                    images.setAttribute('class','col s3 m6 l12');\n                    images.setAttribute('src',element.bild);\n                    inhalt.appendChild(divs);\n                    inhalt.appendChild(images);\n                }, this);\n            }\n        });\n    });\nfunction locationReload(){\n    location.reload();\n}\njQuery.loadScript = function (url, callback) {\n    jQuery.ajax({\n        url: 'https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js',\n        dataType: 'script',\n        success: callback,\n        async: true\n    });\n} </pre> ",
                bild:""
            },
            {
                text:"This is the 'inhalt.js', where I store text and image data",
                bild:""
            },
            {
                text:"<pre class='prettyprint lang-java'>var topicList = [\n     {\n        topic: 'first Topic',\n        body:[\n           /* {\n                text:'',\n                bild:'shots/'\n            }*/\n        ]\n    },\n     {\n        topic: 'second Topic',\n        body:[\n           /* {\n                text:'',\n                bild:'shots/'\n            }*/\n        ]\n    },\n ..etc\n]</pre>",
                bild:""
            },
            {
                text:"The last one is my css, it has nothing special",
                bild:""
            },
            {
                text:"/*css*/\n#fabLabLogo{\n    height: 100%;\n    border-radius:50%;\n    margin-top: 5px;\n}\n#nav1{\n    height: 200px;\n}#nav{\n    height: 100px;\n}\n#dropdown1{\n    margin-top: 64px;\n}\n#header1{\n    display: inline-block;\n    font-size:50px;\n    margin-left: 210px;\n    margin-top: 10px;\n}\n#inputbody{\n    margin-top: 60px;\n}\n#inhalt{\n    margin-top: 70px;\n}\n#breiter{\n    width: 300px;\n}",
                bild:""
            },
            
       ]
        
    },
    {
        topic: "2D & 3D Design",
        body:[
            {
                text:"In this Gif you can see a Project, that i want to make in a Future.\n This is gonna be a 'Rolling Ball' that can roll in every direction",
                bild:"shots/VL2/making2.gif"
            },
            {
                text:"This is the download Page of DraftSight,\n DraftSight is a tool that helps us to module in 2D.",
                bild:"shots/VL2/_dsdownload.png"
            },
            {
                text:"This is a DraftSight menu, during my Work I have used a console and a Tools, that are given above",
                bild:"shots/VL2/_dsmenu.png"
            },
            {
                text:"This is how My Final 2D Work, without measurements looks like",
                bild:"shots/VL2/_mydsproject.png"
            },
            {
                text:"This is how my 2D work looks like with all the measurements.\n I have tryed to do the 'Rolling Ball' as realistic as possible ",
                bild:"shots/VL2/_mydsmeasures.png"
            },
            {
                text:"This is the left side of the 'Rolling Ball' with measures",
                bild:"shots/VL2/_measureleft.png"
            },
            {
                text:"This is the bottom side of the 'Rolling Ball' with measures",
                bild:"shots/VL2/_measurebottom.png"
            },
            {
                text:"This is the right side of the 'Rolling Ball' with measures",
                bild:"shots/VL2/_measureright.png"
            },
            {
                text:"This is the bottom-right side of the 'Rolling Ball' with measures",
                bild:"shots/VL2/_measurebottomright.png"
            },
            {
                text:"Here is the saving format 'dxf', that I can use in Fusion 360 to create a 3D Model'",
                bild:"shots/VL2/_dssavingformat.png"
            },
            {
                text:"Here is the download Link to my 2D Model",
                bild:""
            },
            {
                text:"<a href='downloads/rollin_ball.dxf' download='downloads/rollin_ball.dxf'>Rolling Ball 2D</a>",
                bild:""
            },
            {
                text:"First we have to Downlaod a Fusion 360. \nFusion 360 is a Tool to make a 3D mmodels.\n We can download it free, for learning proposals as students",
                bild:"shots/VL2/fusion360download.png"
            },
            {
                text:"This is how the sketch looks like aufte we import the 'dxf' file that we created in DraftSight",
                bild:"shots/VL2/fusionimport.png"
            },
            {
                text:"After we got a Sketch in Fusion 360, we mostly 'extrude' different surfaces, like it is shown on the picture below.",
                bild:"shots/VL2/fusionextrude.png"
            },
            {
                text:"Sometimes, we have to cut some surfaces from each other.",
                bild:"shots/VL2/fusioncut.png"
            },
            {
                text:"I do had some Issues with creatig wheels at the bottom of the sketch.\nBut I've found some nice Solution: I've copied a wheel from above and modified it.",
                bild:"shots/VL2/fusion2.png"
            },
            {
                text:"After that a have made the 'Holding Border' for it.",
                bild:"shots/VL2/fusion3.png"
            },
            {
                text:"A good programmer says:\n 'Why do we need to do the work again, if we can copy it'. \n So i really did it, I have copyed the other 3 Wheel",
                bild:"shots/VL2/fusion4.png"
            },
            {
                text:"So this is how my 'Rolling Ball' looks like now.",
                bild:"shots/VL2/fusionfinal.png"
            },
            {
                text:"Here is the download Link to my 3D Model",
                bild:""
            },
            {
                text:"<a href='downloads/rollig_ball_(v1).f3d' download='downloads/rollig_ball_(v1).f3d'>Rolling Ball 3D</a>",
                bild:""
            },
            {
                text:"I have also made a GIF with FlashBack, that you can see a the beginning",
                bild:"shots/VL2/flashback.png"
            }
            

           /* {
                text:"",
                bild:"shots/VL2"
            }*/
        ]
    },
    {
        topic: "Laser cutting",
        body:[
            {
                text:"",
                bild:"shots/VL3/compressjpeg/last1-min.jpg"
            },
            {
                text:"In this Lesson we have learned how to Exploit Laser Cutter. \nIn FabLab we have got at least 2 of them, that we are allowed to use.\n The Big One",
                bild:"shots/VL3/compressjpeg/big-min.jpg"
            },
            {
                text:"And the small one",
                bild:"shots/VL3/compressjpeg/small-min.jpg"
            },
            {
                text:"Our assignment for 2 Weeks was to design some pieces in 2d so that they fit in each other and make some engraving \nSo i have desided to make a Chess Board with Chess figures \nBelow you can see my finished Projekt ",
                bild:"shots/VL3/compressjpeg/last2-min.jpg"
            },
            {
                text:"Below you can see the skatches of the pawn i have made",
                bild:"shots/VL3/chesspawnskatch.png"
            },
            {
                text:"Below you can see the skatches of the rock i have made",
                bild:"shots/VL3/chessrockskatch.png"
            },
            {
                text:"Below you can see the skatches of the knight i have made",
                bild:"shots/VL3/chessknightkatch.png"
            },
            {
                text:"Below you can see the skatches of the bishop i have made",
                bild:"shots/VL3/chessbishopskatch.png"
            },
            {
                text:"Below you can see the skatches of the queen i have made",
                bild:"shots/VL3/chessqueenskatch.png"
            },
            {
                text:"Below you can see the skatches of the king i have made",
                bild:"shots/VL3/chesskingskatch.png"
            },
            {
                text:"Now i will tell you how did i made my figures, therefore i will take a bishop as an example \n All my pieces have 46 mm width. \n So I have just copied the bottom skretch from the pawn",
                bild:"shots/VL3/LauferFotos/laufer1.png"
            },
            {
                text:"First I have made a head-curve with a spline and set a head with a simple circle",
                bild:"shots/VL3/LauferFotos/laufer2.png"
            },
            {
                text:"An then I have copied the left shape of the head",
                bild:"shots/VL3/LauferFotos/laufer4.png"
            },
            {
                text:"And mirrored it up to 180 deg",
                bild:"shots/VL3/LauferFotos/laufer6.png"
            },
            {
                text:"After that i have just moved this piece to the left and connected the corners",
                bild:"shots/VL3/LauferFotos/laufer8.png"
            },
            {
                text:"This is how it looks at the end",
                bild:"shots/VL3/LauferFotos/laufer10.png"
            },
            {
                text:"After I have done all the figure it was time to make a board.\n First i made it in DraftSight.\n The squares are 50 mm high and 50 mm wide.",
                bild:"shots/VL3/LauferFotos/board1.png"
            },
            {
                text:"This is how it looked like without any hatch modification.\ There are some additional stripes, those squares were reserved for the numbers and letters",
                bild:"shots/VL3/LauferFotos/board2.png"
            },
            {
                text:"Then I've modified it in Rhino with layers, letters and numbers.",
                bild:""
            },
            {
                text:"<a href='https://www.rhino3d.com/download/rhino/5/latest'>Rhinoceros</a><br> Yellow, blue and green layers stand for Raster and Red for the Vektor.",
                bild:"shots/VL3/boardsketch.png"
            },
            
            {
                text:"After we have all the modells it is time to cut them, therefore we have have two machines that i mentioned earlier\n The global explotation of the machienes are similar.\n First we have to turn it on, set the focus and Jog, turn the 'vacuum cleaner' on, send the From PC and go\n Don't worry I will explain each step below for each machine.\n",
                bild:""
            },

            //-----------------------------------------------
            {
                text:"First I made the Figures. \nFor the figures I've use a small machine\n Okay, fist of all we have to set a printing options, therefore press Ctrl + P and then 'Properties' and it will open this blue window. \n Here You can set a piece size in this machine in inches in the big one in mm. \nOur job type ist Vektoring - Cutting and the Vektor setting are by 80% speed, 100% power and 1700 Freq.",
                bild:"shots/VL3/smallconfig.png"
            },
            {
                text:"Now we have to start a 'small' Laser Cutter.",
                bild:"shots/VL3/compressjpeg/smallpower-min.jpg"
            },    
            {
                text:"After we set the proper settings and started the machine we can go to the machine, and set a focus.\n Go to the machine and press the middle button at the bottom of the menu panel",
                bild:"shots/VL3/compressjpeg/smallmenudone-min.jpg"
            },
            {
                text:"Now the Display should show: 'X/Y Disabled' it means you can change the focus and the jog.",
                bild:"shots/VL3/compressjpeg/smallmenuxy-min.jpg"
            },
            {
                text:"To set a focus, you should use arrows on the menu panel of the mashine. But how do we know, if the focus set properly? \nWell you can see the gray pin on the picture below, this pin should barely touch the surface of your board. \nTo set a jog we have to move the head of the Laser Cutter(small one) manually. On the belt for the x axis and the crossbar for the y axis.",
                bild:"shots/VL3/compressjpeg/smallfocusjog-min.jpg"
            },
            {
                text:"After that press the green button on the machine menu panel, and it should show: 'Set Home/Center'. \nNow we can go to the PC send a file.",
                bild:"shots/VL3/compressjpeg/smallmenuendconfig-min.jpg"
            },
            {
                text:"Now, very Important -> turn the 'vacuum cleaner' of the machine on and then you can start the job",
                bild:"shots/VL3/compressjpeg/smallstartL-min.jpg"
            },
            //-----------------------------------------------
            {
                text:"Now we need to angrave black figures, Therefor I've made a rectangle.\n And puted my figures on the machiene -> they have to fit to the rect. \n It ist one of three sizes shown on picture below\n",
                bild:"shots/VL3/hatch.png"
            },
            {
                text:"Here a the setting for the engravement.",
                bild:"shots/VL3/hatchprop.png"
            },           
            {
                text:"I have done it in a big Laser Cutting machine. \nSo here you can see the emegensy button and the power button of the 'Big-Laser Cutter'",
                bild:"shots/VL3/compressjpeg/bigpoweremg-min.jpg"
            },
            {
                text:"It always takes a time to start a Laser Cutter. Now put your material in Laser Cutter.\n At the left side of the menu panel you can see the arrow to choose the option you wonna do \nSo first we gonna set a focus.Therefore we take the 'triangle' and hang it on the head of the Laser Cutter.",
                bild:""
            },
            {
                text:"Then Close the Door, otherwise we would not be able to do any Modification! \nLook to the menu Panel, select 'focus' with the arrows and move the joystick up and down. \nThe triangle should barely touch the surface of the panel you wonna work with. \nAfter you have set focus, just press on the joystick, and the machine will remember the position.",
                bild:"shots/VL3/compressjpeg/bigmenufocus-min.jpg"
            },
            {
                text:"After that you can choose a job with the arrows and again use the joystick to move the head \nAfter you have set jog, just press on the joystick, and the machine will remember the position.",
                bild:"shots/VL3/compressjpeg/bigmenujog-min.jpg"
            },
            {
                text:"Well now, very Important -> turn the 'vacuum cleaner' of the machine on and then you can start the job",
                bild:"shots/VL3/compressjpeg/bigstartL-min.jpg"
            },
            {
                text:"Now we are ready to send a board as job from a PC to Laser Cutter \nBut first of all we have to set some paramters for the layers\n Again just press Ctrl + P and ist will open you a printing menu\nPress 'Properties' Button\n You can also set a window and workspace in this menu, just like in a small machine\n Here are the setting for the Board\n We are using Layers with different colors so we have to go to color mapping menu an activate color mapping on the top left corner \n On the Picture is the wrong Power for the first 3 Layers for Hatching, I have used 80 \%",
                bild:"shots/VL3/boardcolormapping.png"
            },
            {
                text:"Last step, very Important -> turn the 'vacuum cleaner' of the machine on and then you can start the job\n The Display will also show the approx. time for th whole work(it is now a proper time for of the board)",
                bild:"shots/VL3/compressjpeg/bigmenujob-min.jpg"
            },
            {
                text:"Troubleshooting! Some pieces barely fited to each other. And some pieces(black ones) are to thin, so they don hold each other properly\n But at the end it was done well and we also played a bit chess =)",
                bild:"shots/VL3/compressjpeg/play-min.jpg"
            },
             {
                text:"<a href='downloads/2DSchach-3D/pawn.dxf' download='downloads/2DSchach-3D/pawn.dxf'>Pawn Sketch</a>",
                bild:""
            },
            {
                text:"<a href='downloads/2DSchach-3D/rock.dxf' download='downloads/2DSchach-3D/rock.dxf'>Rock Sketch</a>",
                bild:""
            },
            {
                text:"<a href='downloads/2DSchach-3D/knight.dxf' download='downloads/2DSchach-3D/knight.dxf'>Knight Sketch</a>",
                bild:""
            },
            {
                text:"<a href='downloads/2DSchach-3D/bishop.dxf' download='downloads/2DSchach-3D/bishop.dxf'>Bishop Sketch</a>",
                bild:""
            },
            {
                text:"<a href='downloads/2DSchach-3D/queen.dxf' download='downloads/2DSchach-3D/queen.dxf'>Queen Sketch</a>",
                bild:""
            },
            {
                text:"<a href='downloads/2DSchach-3D/king.dxf' download='downloads/2DSchach-3D/king.dxf'>King Sketch</a>",
                bild:""
            },
            {
                text:"<a href='downloads/2DSchach-3D/board.dxf' download='downloads/2DSchach-3D/board.dxf'>Board Sketch</a>",
                bild:""
            },
          

        ]
    },
    {
        topic: "3D Printing",
        body:[
            {
                text:"In This assignment we have learned how to use 3D Printer.\n Also wie have learn which configurations we should use to print our models corrctly. \nMy Idea was to print a Iron Throne from Game of Thrones ",
                bild:"shots/VL4/final.gif"
            },
            {
                text:"To make a 3D Model I have used Fusion 3D. \n First of all I have made a 'Throne'. \n The Whole Throne is not going to be an exactly copy of an original.",
                bild:"shots/VL4/Throne.png"
            },
            {
                text:"In this Picture you can see a Sword near the 'Throne' I have made on my own.",
                bild:"shots/VL4/Throne2.png"
            },
            {
                text:"So here I have started to copy and change the angle of the swords and place them 'on the 'Throne'",
                bild:"shots/VL4/Throne3.png"
            },
            {
                text:"This is how it finally looked like.",
                bild:"shots/VL4/Throne4.png"
            },
            {
                text:"To configure the printing settings, we have used a Cura. \n But in order to use Cura we have to export our Object with stl.\n I also had some issue with exporting it, because i  hade some Swords that don't wanted to create to the Throne.\n Therefore I have one Component of them and exported it.",
                bild:"shots/VL4/stl.png"
            },
            {
                text:"<a href ='https://ultimaker.com/en/products/ultimaker-cura-software' target='_blank'>Cura</a>",
                bild:""
            },
            {
                text:"My Object was to big so I have to scale it down to 30\%, you can See it on the top-left.",
                bild:"shots/VL4/ThroneU2+.png"
            },
            {
                text:"The 3D Printer I have used, was Ultimaker 2+, the Material was PLA.\n I also wanted to have a good Quality of my object, so I have set 'Layer Heigth' by 0.15 mm.\n 'Wall thikness' was set by 1.3 mm, and 'top/bottom Tikness' was set by 1 mm. - ths settings are self descibable. \nThe Infill destiny was set by 15 %, so that it is not going to waste a lot of material and time.",
                bild:"shots/VL4/ThroneU2+2.png"
            },
            {
                text:"Enable Retraction is checked, so that there is no extrusion of the Material during nozzle moves from one point to another.\nThe Speed and Travel settings were set by 60 and 120, -> nothing special, normal configuration.",
                bild:"shots/VL4/ThroneU2+3.png"
            },
            {
                text:"Now to support setting, this is one of the important part of the settings, because without a support we will have some 'beard' on on the 'Overhang angles'\nSupport Patter -> Zig-Zag, it ist easier to remove it.\n Support line distance = 7",
                bild:"shots/VL4/ThroneU2+4.png"
            },
            {
                text:"In this picture is 'Suport Top Distance' is ot so good.\ I have mentioned it during the documentation.\n I would be better to make bit more, so that i could easily remove it.",
                bild:"shots/VL4/ThroneU2+5.png"
            },
            {
                text:"here You can see the Layers.",
                bild:"shots/VL4/ThroneU2+7.png"
            },
            {
                text:"Okay after I have finished with the configuration, i have to export it as an .gcode file.\n",
                bild:"shots/VL4/gCode.png"
            },
            
            {
                text:"I have already moved the .gcode of the object to the SD-Card.\n Turned the Machine on, the Turn on Button is on the Backside of the Mashine.\n Inserted the SD-Card to the Machine. and started a job, it ist not so difficult\n 14 Hours to print.",
                bild:"shots/VL4/printing.jpg"
            },
            {
                text:"Printing =)",
                bild:"shots/VL4/printing_closer.jpg"
            },
            {
                text:"It almost took 14-15 Hours to print it, and this is how it my Object looks like with supports",
                bild:"shots/VL4/end_printing.jpg"
            },
            {
                text:"",
                bild:"shots/VL4/end_printing2.jpg"
            },
            {
                text:"Finally done it, all supports are removed.",
                bild:"shots//VL4/final.jpg"
            },
            {
                text:"",
                bild:"shots//VL4/final2.jpg"
            },
            {
                text:"<a href='downloads/GoT/Fusion_Throne.f3d' download='downloads/GoT/Fusion_Throne.f3d'>Throne .f3d Obj</a>",
                bild:""
            },
            {
                text:"<a href='downloads/GoT/Throne_new.stl' download='downloads/GoT/Throne_new.stl'>.Stl File</a>",
                bild:""
            },
            {
                text:"<a href='downloads/GoT/UM2_Throne_new.gcode' download='downloads/GoT/UM2_Throne_new.gcode'>.gcode of Throne</a>",
                bild:""
            },
            
          

        ]
    },
    {
        topic: "CNC Machining",
        body:[
            {
                text:"This is the Sword, that i want to mill the Roland CNC Machiene.\nI have copied a model fo the Sword from the Last assignment, where I have made my own Iron Throne.\n I have expand it a bit and adjust it to the size of the 'working Box'",
                bild:"shots/VL5/Sword.gif"
            },
            {
                text:"In oder to work with the CNC Machiene, we have to set up some configuration. \n Therefore we have to change our Mode-view to CAM on the top-left of the Fusion 360.",
                bild:"shots/VL5/cam.png"
            },
            {
                text:"Now we are in CAM view, the next stap is create a new Setup.",
                bild:"shots/VL5/new_setup.png"
            },
            {
                text:"After we have created a new setup, we ca edit it, like this.",
                bild:"shots/VL5/setup_edit.png"
            },
            {
                text:"The first this, we have to configure in setup settings is Stock size, we schuld be quite presize.\n The Mode schould be set to: 'Fixed size box'.\n In some cases you can play with the width, height and depth, but this is for advanced users.",
                bild:"shots/VL5/setup_stock.png"
            },
            {
                text:"Okay now we go to the 'Setup' in Setup window, here we have to configure Orientation of the X and Z axes.\n We have to set the 'Origin' to 'Model boxpoit', so that we cover the whole workin space.\n And in 'Model point', we choose where the machine schould start to work.",
                bild:"shots/VL5/box_point.png"
            },
            {
                text:"After that weh have to choose the kide of the work, which the machine should execute.\n In this assignment we have to make two make two 'Cuts', the rough one and the final.\n For the first 'Cut' we can use an 'Adaptive clearing', choose this option in 3D menu.",
                bild:"shots/VL5/adaptive_clearing.png"
            },
            {
                text:"Okay now we are in 'Adaptive menu' -> Tool\n It ist better for ou proposes to set a 'Spindle Speed' to 10000 rmp, the other setting schould change automaticalli.\n Out machiene doest have a coolant, so whe have to disable this function.\n After that we have to set out tool, therefore just click to the 'tool' -> next steps will be discribed in th next step.",
                bild:"shots/VL5/adaptive_tool.png"
            },
            {
                text:"Okay this is where we adjust our tool.\n For mus purposes it was recomended to take a 'Flat end mill'\n Material -> Carbide, because the Flute was Dark(My own explanation). Carbide ist strong.\n Unit: Millimeters\n Number of flutes was 3\n Diameter : 3. It is very importat to measure the diameter of the 'Working Head'.\n Next ist schaft diameter: 6.00 mm, it ist the diameter of the mill part which ist below the shoulders.\n Flute Length: 9.00 mm -> Flute length end is where the flute changes it's direction.\n Shoulder length: 9.00 mm, is the 'whole' flute length. -> we can set it like flute length.\n The Body length : 30.27 mm, ist the length that ist seen, after our tool being fixed in CNC machine. \n Overall length: 53.00 mm, is the whole length of the tool",
                bild:"shots/VL5/tool.png"
            },
            {
                text:"After that we can check Passes of the adaptive tool.\n We have increased the Tolerance to 2.0, it ist only a rough cut, so we can allow us to that.\n It will save some time.",
                bild:"shots/VL5/adaptive_passes.png"
            },
            {
                text:"Just press Ok,and Fusion will generate the Toolpathes.\n These are lines.\n Blue lines are removing lines and the Yellow ones are Movement lines, without removing anything.",
                bild:"shots/VL5/cncverlauf.png"
            },
            {
                text:"So now we simulate a work.",
                bild:"shots/VL5/simulate.png"
            },
            {
                text:"Durig the simulation, you have to check, that you have no Collisions.",
                bild:"shots/VL5/no_collisions.png"
            },
            {
                text:"Everything is okay, and now we can start with the 'final cut'.\n For our purposes 'contour' is the optimal choice. \n In this project i have coosen the same tool -> Again 'Spindle Speed': 10000 and coolant is turned off.",
                bild:"shots/VL5/final_tool.png"
            },
            {
                text:"In Geometry I have checked a funcktion: AvoidTouch Surfaces, and selected all the surface of the x axis on the Sword.\n I this the funcktion name speeks for it self, and there is no need to discribe it.",
                bild:"shots/VL5/final_geometry.png"
            },
            {
                text:"Here just check th passes of the 'final cut'. Tolerance: 1.00 mm so the cut will be very precize.",
                bild:"shots/VL5/final_passes.png"
            },
            {
                text:"Again, we have to check here, that there are no collisions.",
                bild:"shots/VL5/finalcut_nocollisions.png"
            },
            {
                text:"Now we can export the binary files. To do this, just right-click to the rough and later on final cut and click: Post Process.\n So now you can see this window, here we have to choose our machine, in our case it is Roland RML.\n At the Properties disable -> MDX15or20 and enable MDX40, because we have this model and just post it.",
                bild:"shots/VL5/export.png"
            },
            {
                text:"Got to the CNC Machine and place your Working Space, Important -> it should be fixed good.",
                bild:"shots/VL5/set_the_wp.jpg"
            },
            {
                text:"Now we are at the laptop, that is connected to the Roland.\nHere he have to start the Programm 'VPanel' -> the machine should be on, otherwise the programm will not recognize a machine.",
                bild:"shots/VL5/startmconfig.png"
            },
           
            {
                text:"With the Arrows, that you see in the picture above you can set focus of the machine.\n The focus should be set relative to the 'model Point' in Fusion.\n The Mill should barely touch the edge oth the Working space.",
                bild:"shots/VL5/working_edge.jpg"
            },
            {
                text:"After you have set you focus, just choose: Set XYZ Origin Point under 'Set Origin Point' an click 'apply' button.\n You X,Y,Z values should be '0'.",
                bild:"shots/VL5/xyz.png"
            },
            {
                text:"Now just click 'Cut' button.\n Here you can load your binary codes\n But be sure, that there is no other codes.\ You can also upload 2 codes. First uploaded code -> First executing code.",
                bild:"shots/VL5/uploadbincode.png"
            },
            {
                text:"Here is The Working Process",
                bild:"shots/VL5/workingps.jpg"
            },
            {
                text:"Still Working =)",
                bild:"shots/VL5/workingps2.jpg"
            },
            {
                text:"Remember -> Very Important, you have to leave the CNC Machine clean, after use -> like this.",
                bild:"shots/VL5/clear.jpg"
            },
            {
                text:"Finaly Done it =)",
                bild:"shots/VL5/final.jpg"
            },
            {
                text:"<a href='downloads/Sword/Sword_Ultimate.f3d' download='downloads/Sword/Sword_Ultimate.f3d'>3D Model of the Sword</a>",
                bild:""
            },
            {
                text:"<a href='downloads/Sword/sword.prn' download='downloads/Sword/sword.prn'>Rough Cut</a>",
                bild:""
            },
            {
                text:"<a href='downloads/Sword/sword1.prn' download='downloads/Sword/sword1.prn'>Final Cut</a>",
                bild:""
            },
            
            
        ]
    },
    {
        topic: "Electronics Production",
        body:[
            {
                text:"Yeah, it Blinks.\n The topic of the week is called 'Electronics Production'\nIn this week we learned about the (PCBs) - Printed circuit boards.\n In the assignment for this week we had a PCB-image.\n So the main task was to produce a PCB board from the image.",
                bild:"shots/Vl6/led_final.gif"
            },
            {
                text:"Eagle - is the tool, where we can Design some Boards. \nEagle is also a product of Autodesk, so we can get a free licence as students.",
                hrefs:"<a href ='https://www.autodesk.com/education/free-software/eagle'>Eagle</a>",
                bild:"shots/Vl6/1eagle.png"
            },
            {
                text:"As I told before we had a predisigned boards, that we can download from the git Course. \n Here we open the hello.brd, that wwas prepared for us.",
                bild:"shots/Vl6/2hello_brd.png"
            },
            {
                text:"The Board is now full of unnecessary stuff. To disable their view, we have to disable them in layers. \nThe only layers  we need are: Top and Pads.",
                bild:"shots/Vl6/3layers.png"
            },
            {
                text:"There can be also a next 'problem' we have to handle with. If we Export our picture, we can see some small numbers and writing, that we don't need.\n In order to remove them we have go to Options->Set->Misc and Disable('Enable') everything like in the picture.",
                bild:"shots/Vl6/4displays.png"
            },
            {
                text:"Now we can export our picture. The resolution should be set by 1500dpi.\n Area -'Window'\nMonochrome -Activated.",
                bild:"shots/Vl6/5export.png"
            },
            {
                text:"Here we have opened the exported - PNG Image in GIMP. And selected the working area. \nTo do the selection, just got to tools -> selection tools -> rectngle select.",
                bild:"shots/Vl6/6rect.png"
            },
            {
                text:"Cut the Selection out and open it in a new Window. The shortes way to do this is: File -> Create -> From Clipboard, after you have cut hte image.",
                bild:"shots/Vl6/8cutedimage.png"
            },
            {
                text:"Now do the same here. Go to select  -> Rounden Rectangle, set the Radius by 15 % adn Press okay.\n We do this step, do define th whole size of our PCB",
                bild:"shots/Vl6/12ok.png"
            },
            {
                text:"This is how it looks now.\n Here you have to cut the Selection and export it as png.",
                bild:"shots/Vl6/13cut2.png"
            },
            {
                text:"To fill in the cutted place, just right-click to the hollow area and  ->edit -> fill with white BG color ",
                bild:"shots/Vl6/14white.png"
            },
            {
                text:"Go to To",
                bild:""
            },
            {
                text:"<a href='fabmodules.org'>Fabmodules.org</a> Select the Images and the Machine-Code you are going to work with.<br> Im my case it is a G-codes(.nc). I wanted to work with diffrent machines thats wwhy i have choosen Cirqoid.<br> PCB traces should be st by (1/64).",
                bild:"shots/Vl6/15fabmodules.png"
            },
            {
                text:"Here we can see the settings that we have to change.",
                hrefs:"<a href='downloads/vl6/FabModules_Settings.txt' download='downloads/vl6/FabModules_Settings.txt'>Download Settings.txt</a>",
                bild:"shots/Vl6/16FabModules.png"
            },
            {
                text:"Here we can see the NetBeans programm with which we schould set the focus.\n It is rather simple, thats why i will jump thisstep over.",
                bild:"shots/Vl6/q-min.jpg"
            },
            {
                text:"Here we have also a scematic, how we have to solder ou board.",
                bild:"shots/Vl6/eagle_pinout.jpeg"

            },
            {
                text:"Okay, this is how my Boad looks like.",
                bild:"shots/Vl6/chip-min.jpg"
            },
            {
                text:"Okay, now we Lauch an Arduino IDE, you can download it from here.",
                hrefs:"<a href ='https://www.arduino.cc/en/Main/Software'>Arduino</a>",
                bild:"shots/Vl6/downloadarduino.png"
            },
            {
                text:"Okay, now we have to program our board. So first connect the board to the Laptop(PC).\n Here is the txt-file with the whole documentation.",
                hrefs:"<a href='downloads/vl6/how-to-program.txt' download='downloads/vl6/how-to-program.txt'>Download How-to-Program.txt</a>",
                bild:"shots/Vl6/connect2-min.jpg"
            },
            {
                text:"Now we have to to install an ATtiny Board, to do this, first google to ATtiny -> this should be the first page. Now copy the shown url there.",
                bild:"shots/Vl6/ard4.png"
            },
            {
                text:"Got to File -> Perferences.",
                bild:"shots/Vl6/ard5.png"
            },
            {
                text:"Pase the cpoed url to: Additional Boards Manager URLs.",
                bild:"shots/Vl6/ard6.png"
            },
            {
                text:"In Boards Manager Window seach for ATtiny and install a package.",
                bild:"shots/Vl6/ard8.png"
            },
            {
                text:"Now Go to Tools -> Board -> Board Manager",
                bild:"shots/Vl6/ard1.png"
            },
            {
                text:"Under file->examples find and open the arduino as isp sketch",
                bild:"shots/Vl6/isp_sketch.png"
            },
            {
                text:"Upload the sketch to the arduino",
                bild:"shots/Vl6/upload.png"
            },
            {
                text:"Disconnect the arduino from the pc",
                bild:"shots/Vl6/connect1-min.jpg"
            },
            {
                text:"Connect the hello board with the arduino (check the connection schema) - triple check the connections.\n Very Important! Connect Wires onlay if arduino disconnected from the LapTop(PC)",
                bild:"shots/Vl6/connect3-min.jpg"
            },
            {
                text:"Connect the arduino to the pc",
                bild:"shots/Vl6/connect4-min.jpg"
            },
            {
                text:"Select the right board -> attiny25/45/85",
                bild:"shots/Vl6/ard9.png"
            },
            {
                text:"Select the Rigght Port",
                bild:"shots/Vl6/ard2.png"
            },
            {
                text:"Select the Right Processor -> attiny45",
                bild:"shots/Vl6/ard10.png"
            },
            {
                text:"Select the Right Clock -> internal 8mhz",
                bild:"shots/Vl6/ard11.png"
            },
            
            {
                text:"Under tools select the arduino as isp programmer",
                bild:"shots/Vl6/ard12.png"
            },
            
            {
                text:"Double check all the paramters  - click to tools-> burn bootloader",
                bild:"shots/Vl6/ard13.png"
            },
            {
                text:"Now we can write our own programm.\n But our assignment for the week was to make an LED blink, so we loaded an example from: \nFile->Examples->Basics->Blink.",
                bild:"shots/Vl6/ard3.png"
            },
            {
                text:"Here we have just to change a 'LED_BUILTIN' variable to '2'.",
                bild:"shots/Vl6/ard14.png"
            },
            {
                text:"Now Run it with: Sketch -> Upload Using Programmer",
                bild:"shots/Vl6/ard15.png"
            },
            
        ]
    },
    {
        topic: "Electronics Design",
        body:[
            {
                text:"In this lesson we should modify a Board with adding an LED an button on it.\n I have choosen a 'satshakit-board'(very difficult name to spell out).\nThere was also a requirement for this week to include some button and LED.\n So have added a button an an LED on digital pins 5 and 6.\n For the LED we also need a 499 Ohm Resistor connected to the GND.\n For the button we need a 10k Resistor connected to the GND where the arduino pin attached and vcc at the opposite side.",
                bild:"shots/VL7/1sketch.png"
            },
            {
                text:"So here is the part on a scematic i have modified.",
                bild:"shots/VL7/2sketchmodif.png"
            },
            {
                text:"This i how the Board looked like after the modification\n I have modified the Board this way, because -> \n I dont wanted to change the actual Positioning of the satschakit, it is very good\n And the 2 Reason, it was the Simplies way to modify a Board.",
                bild:"shots/VL7/3_0board.png"
            },
            {
                text:"Here ist a black and white image of my board,\n In this week i will not explain you how to do this,\n i have do it the week before. ",
                bild:"shots/VL7/3_1whole.png"
            },
            {
                text:"Okay, done I have engraved, cutted out and soldered my board.",
                bild:"shots/VL7/4board.jpg"
            },
            {
                text:"Okay These are connection, that i have to use to burn a bootloader.",
                bild:"shots/VL7/5satshakitarduino.png"
            },
            {
                text:"Here I have done it, the LED showing us, that everything seems to be okay.",
                bild:"shots/VL7/6connected.jpg"
            },
            {
                text:"I have Satshakit(again, it's had to spell it). \nIt is like an arduino board, so to programm it a have to use an ftda-cable 5v,\n and connect the this way.",
                bild:"shots/VL7/7_0connectftdasch.jpg"
            },
            {
                text:"<pre class='prettyprint lang-java'> void setup() {\npinMode(6, OUTPUT); \npinMode(13, OUTPUT); \n} \nvoid loop() { \ndigitalWrite(6, HIGH); \ndigitalWrite(13, HIGH); \ndelay(1000); \ndigitalWrite(6, LOW); \ndigitalWrite(13, LOW); \ndelay(1000); \n}</pre> ",
                //bild:"shots/"
            },
            {
                text:"So above you can see a code for the Video, that is below. It modefied blink sketch, to check, if everything is fine.",
               // bild:"shots/",
                video:"shots/VL7/7_1blink.mp4"
            },
            {
                text:"<pre class='prettyprint lang-java'>const int buttonPin = 5;\nconst int ledPin =  13; \nconst int ledPin2 =  6;  \nint buttonState = 0; \nvoid setup() { \n pinMode(ledPin, OUTPUT);\n pinMode(ledPin2, OUTPUT);\n pinMode(buttonPin, INPUT);\n}\nvoid loop() {\n buttonState = digitalRead(buttonPin);\n if((buttonState == HIGH)){\n digitalWrite(ledPin, HIGH);\n digitalWrite(ledPin2, HIGH);\n delay(1000); \n digitalWrite(ledPin, LOW);\n digitalWrite(ledPin2, LOW);\n delay(1000);\n } else {\n digitalWrite(ledPin, HIGH);\n delay(1000);\n digitalWrite(ledPin, LOW);\n delay(1000);\n }\n}</pre>",
                //bild:"shots/"
            },
            {
                text:"Okay, here Is the Assigmnet code above.\n A part of the assignment of the week was to controll LED with a BUTTON.\n Thats it=)",
                //bild:"shots/",
                video:"shots/VL7/7_2button.mp4"
            },
            {
                text:"<a href='downloads/satshakit_edit/engrave.png' download='downloads/satshakit_edit/engrave.png'>Engraving</a>",
               // bild:""
            },
            {
                text:"<a href='downloads/satshakit_edit/cut.png' download='downloads/satshakit_edit/cut.png'>Cut</a>",
                //bild:""
            },
            {
                text:"<a href='downloads/satshakit_edit/satshakit_cnc.brd' download='downloads/satshakit_edit/satshakit_cnc.brd'>Board</a>",
                //bild:""
            },
            {
                text:"<a href='downloads/satshakit_edit/satshakit_cnc.sch' download='downloads/satshakit_edit/satshakit_cnc.sch'>Schematic</a>",
               // bild:""
            },
            
           
            
        ]
    },
    {
        topic: "Embedded Programming",
        body:[
            {
                text:"!Warning!: In Arduino String 'String' ist actually in Doublequotes",
            },
            {
                text:"<pre class='prettyprint lang-java'>int incomingByte = 0;\nint clearint;\nint var1;\nint var2;\nunsigned long fibZahl = 0;\nunsigned long zahl = 0;\nbyte arr[] = {-1,-1,-1,-1,-1,-1,-1,-1} ;\nint counter = 0;\nint serialcounter = 0;\nlong milend;\nlong milstart ;\n\n\n\nlong milenditt;\nlong milstartitt ;\n\n\n\n\n\nconst int ledPin =  13;  \nconst int ledPin2 =  6; \n// Okay Here we Go\n\nvoid setup() {\n        pinMode(ledPin, OUTPUT);\n        pinMode(ledPin2, OUTPUT);\n        Serial.begin(9600);     // opens serial port, sets data rate to 9600 bps\n}\n//------------------------------------------\nunsigned long fib(unsigned long n){ // This is a Recursive Fib Function (Remember it from the Algorithm and Datastructure Course)\n        if (n<2){\n            return n;\n        }\n        unsigned long f1 = fib(n-1);\n        unsigned long f2= fib(n-2);\n        return f1+f2;\n}\n//-------------------------------------------\nunsigned long fibonacciIterative(unsigned long n) { //// This is a Recursive Fib Function Copied From the Internet=)\n    if(n <= 1) {\n      return n;\n    }\n    unsigned long fib = 1;\n    unsigned long prevFib = 1;\n    \n    for(unsigned long i=2; i<n; i++) {\n      unsigned long temp = fib;\n      fib+= prevFib;\n      prevFib = temp;\n    }\n    return fib;\n  }\n//---------------------------------------------\n\n\nvoid loop() {\n        // send data only when you receive data:\n        /**/if (Serial.available() > 0) {\n                incomingByte = Serial.read();\n                if(incomingByte >48 && incomingByte < 58){ /*Proceed only if the Number lay Between 0-9 BTW.\n                  have found a way to read multiple numbers, but i have no time to modify a code*/\n                  proceed(incomingByte-48);\n                }\n             \n             }       \n}\n\nunsigned long concat1(int x, int y) { // this is the Concatenation Function of two Ints\n    unsigned long temp = y;\n    while (y != 0) {\n        x *= 10;\n        y /= 10;\n    }\n    return x + temp;\n}\n\n\nvoid proceed(unsigned long n){\n    \n              //---------------concatenate---------------------------\n               zahl = n;\n              //------------------------------------------\n            \n                 \n                  Serial.print('I received: ');\n                  Serial.println(zahl);\n\n              //---------------------calculating------------\n\n                   milstart = micros(); // I also wanted to findout, how much it will take to Calculate Req and Iter.\n                   fibZahl = fib(zahl); // run the recursive fibnoccci function\n                   milend = micros();\n                   long diff = milend - milstart; // here we count a difference between start and the end of the recursive fibonacci function\n                  \n               //--------------------------------------------    \n                   \n                    milstartitt = micros(); // again start the timer\n                    fibZahl = fibonacciIterative(zahl); // run the iterrative fiboncaii function\n                    milenditt = micros();\n                    long diff2 = milenditt - milstartitt;// here we count a difference between start and the end of the iterrative fibonacci function\n\n\n                     Serial.print('Calculation time Req.: ');\n                     Serial.println(diff);\n                     Serial.print('Calculation time Itt: ');\n                     Serial.println(diff2);\n\n                     // So what i Found out ist, that Arduino Programming Environment needs a big amout of time to call a function\n               //---------------------calculating------------\n\n                  \n                  \n                  Serial.print('Fibonacci: ');\n                  Serial.println(fibZahl);\n\n                  ledlight();  \n}\n\nvoid decToBinary(int num) // This is the Decimal to Bin Function (Written by myself)\n{\n    if(num == 0)\n        return ;\n    decToBinary(num / 2);\n    arr[counter] = num % 2;\n    counter++;\n\n}\n\nvoid ledlight(){\n  decToBinary(fibZahl); // here we call the fkt above\n for(int i = 0; i < 8; i++){\n   if(arr[i] != 255){\n      Serial.print(arr[i]); // and give it to the Serial console\n    }\n    \n }\n Serial.println(' ');\n\n /* now in Array 'arr' we have a binary number if x'th fibonacci number.\n my idea was to give this number with the helf of 2 leds. \n When we meet a '1' 2 LED's are HIGH and '0' -> only 1*/\n  for(int i = 0; i < 8; i++){\n    if(arr[i] == 1){ // so here we read a 1 as an input\n      digitalWrite(ledPin, HIGH);// I give a power to the 1st LED -> pin 13\n      digitalWrite(ledPin2, HIGH); // And here i hive a powoer to the 2nd LED -> pin 6\n      delay(1000); //we let the LED's run for 1 second \n      digitalWrite(ledPin, LOW);// and in this line and at the line below we turn the LEDs off\n      digitalWrite(ledPin2, LOW);\n      delay(1000);\n    }else if(arr[i] == 0){ // if we read a 0 \n      digitalWrite(ledPin, HIGH);// we give a pover to the 1st LED -> pin 13\n      digitalWrite(ledPin2, LOW); // and let the power off, to the 2nd one (Actually we don't need this line, it is only for good understandig), \n      delay(1000); // here we let out 1 LED on for 1 sek\n      digitalWrite(ledPin, LOW); // and here we turn it off\n      digitalWrite(ledPin2, LOW); // Actually we don't need this line too, it is only for good understanding\n      delay(1000);\n    }\n      \n  }\n  clearfkt(); // \n}\n\nvoid clearfkt(){ // here is the clearfkt, it allows us to take a multiple input.\nincomingByte = 0;\nvar1 = clearint;\nvar2 = clearint;\nfibZahl = clearint;\ncounter = 0;\nserialcounter = 0;\nmilend = clearint;\nmilstart = clearint;\nmilenditt = clearint;\nmilstartitt = clearint;\n  for(int i = 0; i < 8; i++){\n    arr[i] = -1;\n   }\n\n\n}\n\n\n </pre>",
                //bild:"shots/"
            },
            {
                text:"Okay above you can see a pretty long code, i will comment it in the nearest future.\n But for now it is there, so if you want to make something like: Type a Fibonacci-number you want to see, from 1 - 9. \n And you will se an Output in Serial like this,\n After that, the fibonacci umber going to be translatet with the leds in binary:\n For 0 - 1 LED and for 1 - 2 LED's",
                bild:"shots/VL8/8_1fib.png"
            },
            {
                text:"So here ist the Video of 9th Fibonacci number (100010)",
                video:"shots/VL8/8_2fib.mp4"


            },
            
        ]
    },
    {
        topic: "Input Device",
        body:[
            {
                text:"In this assignment, we need to use an input device. I have used a  PM.\nA potentiometer is a simple knob that provides a variable resistance, which we can read into the Arduino board as an analog value.",
                bild:"shots/VL9/9input.png"
            },
            {
                text:"We connect three wires to the Arduino board. The first goes to ground from one of the outer pins of the potentiometer. \nThe second goes from 5 volts to the other outer pin of the potentiometer. \nThe third goes from analog input from Arduino to the middle pin of the potentiometer.",
                bild:"shots/VL9/9conn.png"
            },
            {
                text:" By turning the shaft of the potentiometer, we change the amount of resistence on either side of the wiper which is connected to the center pin of the potentiometer. This changes the relative 'closeness' of that pin to 5 volts and ground, giving us a different analog input. \n\nWhen the shaft is turned all the way in one direction, there are 0 volts going to the pin, and we read 0.When the shaft is turned all the way in the other direction, there are 5 volts going to the pin and we read 1023.In between, analogRead() returns a number between 0 and 1023 that is proportional to the amount of voltage being applied to the pin.\n\nI have devided this Range to 6 LED, and also uset is as a Frequence. With -> delay(val)",
                bild:"shots/VL9/10_0input.jpg"
            },
            
            {
                text:"<pre class='prettyprint lang-java'> int potPin = 2;    // select the input pin for the potentiometer\n int ledPin = 13;\n int ledPin2 = 12;\n int ledPin3 = 11;\n int ledPin4 = 10;\n int ledPin5 = 9;\n int ledPin6 = 8;// select the pin for the LED\n int val = 0;       // variable to store the value coming from the sensor\n \n void setup() {\n    Serial.begin(9600);\n   pinMode(ledPin, OUTPUT);\n   pinMode(ledPin2, OUTPUT);\n   pinMode(ledPin3, OUTPUT);\n   pinMode(ledPin4, OUTPUT);\n   pinMode(ledPin5, OUTPUT);\n   pinMode(ledPin6, OUTPUT);\n   // declare the ledPin as an OUTPUT\n }\n \n void loop() {\n   val = analogRead(potPin); \n    Serial.println(val);// read the value from the sensor\n    if(val < 171){\n     digitalWrite(ledPin2, LOW);\n     digitalWrite(ledPin3, LOW);\n     digitalWrite(ledPin4, LOW);\n     digitalWrite(ledPin5, LOW);\n     digitalWrite(ledPin6, LOW);\n     \n     digitalWrite(ledPin, HIGH);  // turn the ledPin on (Only 1)\n     delay(val);          // stop the program for some time\n     digitalWrite(ledPin, LOW);\n     delay(val);   \n     }\n \n     if(val > 171 && val < 341){\n     digitalWrite(ledPin, LOW);\n     digitalWrite(ledPin3, LOW);\n     digitalWrite(ledPin4, LOW);\n     digitalWrite(ledPin5, LOW);\n     digitalWrite(ledPin6, LOW);\n \n       \n     digitalWrite(ledPin2, HIGH);  // turn the ledPin on\n     delay(val);          // stop the program for some time\n     digitalWrite(ledPin2, LOW);\n     delay(val);   \n     }\n \n     if(val > 341 && val < 511){\n     digitalWrite(ledPin, LOW);\n     digitalWrite(ledPin2, LOW);\n     digitalWrite(ledPin4, LOW);\n     digitalWrite(ledPin5, LOW);\n     digitalWrite(ledPin6, LOW);\n       \n     digitalWrite(ledPin3, HIGH);  // turn the ledPin on\n     delay(val);          // stop the program for some time\n     digitalWrite(ledPin3, LOW);\n     delay(val);   \n     }\n \n     if(val > 511 && val < 681){\n     digitalWrite(ledPin, LOW);\n     digitalWrite(ledPin2, LOW);\n     digitalWrite(ledPin3, LOW);\n     digitalWrite(ledPin5, LOW);\n     digitalWrite(ledPin6, LOW);\n       \n     digitalWrite(ledPin4, HIGH);  // turn the ledPin on\n     delay(val);          // stop the program for some time\n     digitalWrite(ledPin4, LOW);\n     delay(val);   \n     }\n \n     if(val > 681 && val < 851){\n     digitalWrite(ledPin, LOW);\n     digitalWrite(ledPin2, LOW);\n     digitalWrite(ledPin3, LOW);\n     digitalWrite(ledPin4, LOW);\n     digitalWrite(ledPin6, LOW);\n       \n     digitalWrite(ledPin5, HIGH);  // turn the ledPin on\n     delay(val);          // stop the program for some time\n     digitalWrite(ledPin5, LOW);\n     delay(val);   \n     }\n \n     if(val > 851 && val < 1204){\n     digitalWrite(ledPin, LOW);\n     digitalWrite(ledPin2, LOW);\n     digitalWrite(ledPin3, LOW);\n     digitalWrite(ledPin4, LOW);\n     digitalWrite(ledPin5, LOW);\n       \n     digitalWrite(ledPin6, HIGH);  // turn the ledPin on\n     delay(val);          // stop the program for some time\n     digitalWrite(ledPin6, LOW);\n     delay(val);   \n     }\n    // turn the ledPin off\n                     // stop the program for some time\n }\n </pre>",
                //bild:"shots/"
            },
            {
                text:"So as you can see, the Intensity and the direction of the lights changes, whenever i interact with ...",
                video:"shots/VL9/10_1input.mp4"
            },
            
        ]
    },
    {
        topic: "Output Device",
        body:[
            {
                text:"So this is the Output Assignment. I have decided to choose an LCD 16x2, because I will need this in my Final-Project.\n",
                bild:"shots/VL10/lcdl.jpg"
            },
            {
                text:"The Real name of LCD is lcm-s01602dtr/m\n Datasheet:",
                hrefs:"<a href ='https://www.lumex.com/content/files/ProductAttachment/LCM-S01602DTR-M.pdf'>lcm-s01602dtr/m</a>",
                bild:"shots/VL10/LCD.png"
            },
            {
                text:"I had some Problems with Understandig of the connection, after I found this table(also commented by me)/\n I have finally understood how does it works.",
                bild:"shots/VL10/LCD1.png"
            },
            {
                text:"To Buid a code, I head to read a LiquidCrystal() Library.\n Very useful documentation.\n Btw. All my Connections, are written as a comment.",
                hrefs:"<a href ='https://www.arduino.cc/en/Reference/LiquidCrystalConstructor'>lcm-s01602dtr/m</a>",
            },
            {
                text:"!Warning!: In Arduino String 'String' ist actually in Doublequotes",
            },
            {
                text:"<pre class='prettyprint lang-java'>#include &ltLiquidCrystal.h&gt \n// Connections:\n// LCD pin 3 for contrants to GND, good for beginners\n// rs (LCD pin 4) to Arduino pin 12\n// rw (LCD pin 5) to Arduino pin 11\n// enable (LCD pin 6) to Arduino pin 10\n// LCD pin 15 to Arduino pin 13\n// LCD pins d4, d5, d6, d7 to Arduino pins 5, 4, 3, 2\nLiquidCrystal lcd(12, 11, 10, 5, 4, 3, 2);\nvoid setup(){\n  lcd.begin(16,2);              // columns, rows.  use 16,2 for a 16x2 LCD, etc.\n  lcd.clear();                  // start with a blank screen\n  // if you have a 4 row LCD, uncomment these lines to write to the bottom rows\n  // and change the lcd.begin() statement above.\n  //lcd.setCursor(0,2);         // set cursor to column 0, row 2\n  //lcd.print('Row 3');\n  //lcd.setCursor(0,3);         // set cursor to column 0, row 3\n  //lcd.print('Row 4');\n}\nvoid loop(){\n  lcd.setCursor(0,0);           // set cursor to column 0, row 0 (the first row)\n  lcd.print('Output Device');    // change this text to whatever you like. keep it clean.\n  lcd.setCursor(0,1);           // set cursor to column 0, row 1\n  lcd.print('Fundamentals(ER)');\n}\n </pre>",
                //bild:"shots/"
            },

            {
                text:"Finally Done it, it Works xD",
                bild:"shots/VL10/LCD3.jpg"
            }
        ]
    },
    {
        topic: "Networking",
        body:[
            {
                text:"In this Assignment we had to connect two Board. \nAgain in this case i have choosen the connection, that i will need in my Final Project.\n A Serial Connection.\n Serial communication is the process of sending data one bit at a time, sequentially, over a communication channel. \nIt uses two wires to implement an asynchronous full duplex communication, but only between two devices.\n Remember that in any network you have always to share the ground between the microcontrollers!\n In the Previous assignment I had an LCD.\n So this time I told the Arduino to Send a Message to PCB, an PCB should display it on LCD.\n Each important part of the code ist commented, so there is no need to do this again.",
            },
            {
                text:"!Warning!: In Arduino String 'String' ist actually in Doublequotes",
            },            
            {
                text:"This Code Belongs to the PCB connected with the LCD 16x2.",
            },
            {
                text:"<pre class='prettyprint lang-java'>#include &ltLiquidCrystal.h&gt \n#include &ltSoftwareSerial.h&gt\nSoftwareSerial mySerial(0,1);\n\n// This is the PCB Satshakit Code with LCD and Arduino-Communication\nLiquidCrystal lcd(12, 11, 10, 5, 4, 3, 2); //We talked about a lcd last Lesson\nvoid setup() {\n  mySerial.begin(9600);         // Declare Baudrate\n  lcd.begin(16,2);              // columns, rows.  use 16,2 for a 16x2 LCD, etc.\n  lcd.clear();                  // start with a blank screen\n}\n\nvoid loop() {\n\n  if(mySerial.available()>0){\n     lcd.setCursor(0,0);           // set cursor to column 0, row 0 (the first row)\n     lcd.print(mySerial.readString());  //First ROW ist the Changing Message form the Arduino\n     lcd.setCursor(0,1);           // set cursor to column 0, row 1\n     lcd.print('From Arduino UNO'); //Second Row is the Constant ROW, not from Arduino\n  }\n}</pre>",
                //bild:"shots/"
            },            
            {
                text:"This Code Belongs to the Arduino.",
            },
            {
                text:"<pre class='prettyprint lang-java'>#include &ltSoftwareSerial.h&gt \nSoftwareSerial mySerial(A0,A1); //Declare mySerial at ports 0,1 RX and TX\n\nvoid setup() {\n  mySerial.begin(9600); // Declare Baudrate \n}\n\nvoid loop() {\n  mySerial.println('  Hello PCB(1)  ');//Send this Message to Serial\n  delay(1500);//Delay between Messages\n  mySerial.println('  Hello PCB(2)  ');//Send this Message to Serial\n  delay(1500);//Delay between Messages\n  mySerial.println('  Hello PCB(3)  ');//Send this Message to Serial\n  delay(1500);//Delay between Messages\n}\n</pre>",
                //bild:"shots/"
            },
            {
                text:"Finally Done it, it Works xD",
                video:"shots/VL11/video.mp4"
            }
        ]
    },
    {
        topic: "Final Project",
        body:[
            {
                text:"During whole fundamentals we have learned how to Use Laser Cutter, 3D Printer, CNC Milling-Machine. We have also gained skill at designing and soldering our own PCB-Boards. We have also learned how to control different Input-Output devices build our own communication between PCBs etc. And now with aour final Project we have to demostrate the things we have learned.\n\n It was hard to me to decide what I should do as my final Project. But after a week of googling I have decided to do a CoctailMachine.\n\n During the Assigment 'Electronics Design' I have made a modified version A Satschakit. Satschakit - It is a board by Daniele Ingrassia that Uses ATmega328p microcotoller which is also compareable with Arduino Uno, that uses ATmega328 microcontroller.  It has 14 digital input/output pins (of which 6 can be used as PWM outputs), 6 analog inputs and 16 MHz Frequence. Operating Voltage 5v and Current 200 mA\n\n So here Is a Sketch of my Board, as I already mentioned before, I have modified it a bit inorder to fit the Assignment requirements:\n\nAn have added a button an an LED on digital pins 5 and 6",
                bild:"shots/VL7/1sketch.png"
            },
            {
                text:"This is the board view.",
                bild:"shots/VL7/3_0board.png"
            },
            {
                text:"And this is the PCB Board itself.",
                bild:"shots/VL7/4board.jpg"
            },
            {
                text:"Okay another and the most Important component of the PCB board ist the WaterPump.\n I haven't googled alot and have choosen a Pump with DC RS-360SH Motor and a tube for it. Here is a Link:",
                hrefs:"<a href ='http://www.robotstorehk.com/motors/doc/rs_360sh.pdf'>Datasheet rs-360sh-2885</a>",
                bild:"shots/Final/motor.jpg"
            },
            {
                text:"The first Idea was to run them with a Darlington Array UNL2803. But the Problem was that the single output of an Array lies by 500 mA.\n But to run my Motors I need to have at least 1.75 A(read the documentation above)",
                hrefs:"<a href ='http://www.ti.com/lit/ds/symlink/uln2803a.pdf'>ULN2803A</a>",

                bild:"shots/Final/ULN_2803A.jpg"
            },
            {
                text:"So Daniel advised me to use a BDX53C transistor to operate a RS-360SH-2885 Motor.\n That was quite good decision, because with this transistor I can handle needed 1.75 A.",
                hrefs:"<a href ='http://www.farnell.com/datasheets/1699990.pdf?_ga=2.164868287.1495254907.1517266672-1800106665.1517266672'>BDX53C Transisotor</a>",

                bild:"shots/Final/BDX53C.jpg"
            },
            {
                text:"According to this Picture I have connected my RS-360SH-2885 Motor to the BDX53C.",
                bild:"shots/Final/help.jpg"
            },
            {
                text:"I have also a Diode in order not to damage an Adruino and 10 nF Capacitor to avoid a noise.\n The main Idea is to connect all Motors in the way it is shown there, but with th exeption, that all of the Motors connected in parallel to the power.\n With this architecture I can control motors one by one with only one Power supply with 10V and 1.75 A",
                bild:"shots/Final/Motor_Circuit.png"
            },
            {
                text:"I needed to have some output so I decided to use LCD16x2 lcm-s01602dtr/m,",
                hrefs:"<a href ='https://www.lumex.com/content/files/ProductAttachment/LCM-S01602DTR-M.pdf'>lcm-s01602dtr/m</a>",
                bild:"shots/VL10/lcdl.jpg"
            },
            {
                text:"This is the connection I have Used for the LCD16x2 lcm-s01602dtr/m.\n My Idea was to contol all Motors with the Arduino and an LCD with a Satshakit,\n so don't forget check the pinout of Satshakit while connecting LCD.",
                bild:"shots/VL10/LCD1.png"
            },
            {
                text:"To communicate between the Arduino and PC Board I have used following connection:\nArduino Analog pin 0 to TX of PCB\n Arduino pin 1 to RX of PCB.\n I have used Analog Pins and not RX and TX in order to Programm it without disconnecting any wires.\n ",
                bild:"shots/Final/conn.png"
            },
            {
                text:"From Now on I can control Motors and LCD so I need a Case to put everything together.\nI have used a MakerCase Site to Design my Box in general an modified it with rhino.\n Makercase is a vary useful website where you can create a box for your need with the following parameters: Width, Height, Depth.",
                hrefs:"<a href ='https://www.makercase.com'>MakerCase</a>",
                bild:"shots/Final/makercase.png"
            },
            {
                text:"Here you can see a Schematic that I used to Cut my Box, but it was not Perfect\n I have missed a hole for the cable and also made the width of the attaching holes 5mm, while it should be 6mm.\n Why 6mm? because I was using a MDF 6 mm.",
                bild:"shots/VL10/Made.png"
            },
            {
                text:"And here is the Modified Version, that I have made after I have faced the Problems",
                bild:"shots/VL10/Modif.png"
            },
            {
                text:"I Also made a Cup for my Machine xD.\n The dimension of the Cup were to big, I reduced it later by 65 % in Cura.",
                bild:"shots/Final/final.gif"
            },
            {
                text:"After All I connected everything together and this is how it looks from the outside.",
                bild:"shots/Final/aussen.png"
            },
            {
                text:"And this is the inner View.",
                bild:"shots/Final/innen.png"
            },
            {
                text:"Now we have to Programm it. The code Below Belogs to the PCB.\n It becomes a message from the Arduino Uno and show it on LCD Display.\n The Code is commented, so that you can understand it.\n\n!Warning!: String 'String' ist actually in Doublequotes",
            },
            {
                text:"<pre class='prettyprint lang-java'>#include &ltLiquidCrystal.h&gt\n#include &ltSoftwareSerial.h&gt\nSoftwareSerial mySerial(0,1);\n\n// This is the PCB Satshakit Code with LCD and Arduino-Communication\nLiquidCrystal lcd(12, 11, 10, 5, 4, 3, 2); //We talked about a lcd last Lesson\nvoid setup() {\n  mySerial.begin(9600);         // Declare Baudrate\n  lcd.begin(16,2);              // columns, rows.  use 16,2 for a 16x2 LCD, etc.\n  lcd.clear();                  // start with a blank screen\n}\n\nvoid loop() {\n\n  if(mySerial.available()>0){\n     lcd.setCursor(0,0);           // set cursor to column 0, row 0 (the first row)\n     lcd.print(mySerial.readString());  //First ROW ist the Changing Message form the Arduino\n     lcd.setCursor(0,1);           // set cursor to column 0, row 1\n     lcd.print('  Kali-Fab-Lab  ''); //Second Row is the Constant ROW, not from Arduino\n  }\n}\n</pre>",
                //bild:"shots/"
            },
            {
                text:"Now we have to Programm it. The code Below Belogs to the Arduino UNO.\n I have Also added some buttons to choose the Coctail and to give it.\n The Code is commented, so that you can understand it.\n\n!Warning!: String 'String' ist actually in Doublequotes",
            },
            {
                text:"<pre class='prettyprint lang-java'>#include &ltSoftwareSerial.h&gt\nSoftwareSerial mySerial(A0,A1); //Declare mySerial at ports 0,1 RX and TX\n\n\nint check[3] = {0, 0, 1}; // an array of status, which of 3 motors should be activated.(I have implemented Only 3 Motors)\n\nint back = 11; // A left Button Pin Number\nint give = 12; // A buddle Butten Pin Number\nint forw = 13; // A right Button Pin Number\n\nvoid setup() {\n  Serial.begin(9600); // Serial to debug purposes\n  //3 5 6 9\n  pinMode(3, OUTPUT); // Declare The Output for the Motors(Pumps)\n  pinMode(5, OUTPUT); \n  pinMode(6, OUTPUT); \n  pinMode(9, OUTPUT); \n  mySerial.begin(9600); // Serial to send a message to PCB at 9600 baudrate.\n  pinMode(back, INPUT);\n  pinMode(give, INPUT);\n  pinMode(forw, INPUT);\n  //A1,A2,A3 Buttons\n  \n}\n\nvoid loop() {\n\n\n  if(digitalRead(back)){ // If the Left Button is pressed\n    dec(); // an Array Dec function [0,0,1] -> [0,1,0] all combinations\n    Serial.println(check[0]); // \n    Serial.println(check[1]);\n    Serial.println(check[2]);\n    delay(1000);\n    if(check[0] == 1){\n      mySerial.print('  Whisky-Lemon! ');// Send to Serial this message.\n    }else if(check[1] == 1){\n      mySerial.print('  Vodka-Lemon!  ');// Send to Serial this message.\n    }else if(check[2] == 1){\n      mySerial.print('   Gin-Lemon!   ');// Send to Serial this message.\n    }\n  }\n\n\n  \n  if(digitalRead(give)){ // If the Middle Button is pressed \n    shot(); // give us a shot\n    delay(1000);\n  }\n  if(digitalRead(forw)){// If the Right Button is pressed\n    inc();// an Array Inc function [0,0,1] -> [0,1,0] all combinations\n    Serial.println(check[0]);\n    Serial.println(check[1]);\n    Serial.println(check[2]);\n    if(check[0] == 1){\n      mySerial.print('  Whisky-Lemon! ');// Send to Serial this message.\n    }else if(check[1] == 1){\n      mySerial.print('  Vodka-Lemon!  ');// Send to Serial this message.\n    }else if(check[2] == 1){\n      mySerial.print('   Gin-Lemon!   ');// Send to Serial this message.\n    }\n    delay(1000);\n  }\n}\n\n\n\nvoid inc(){// an Array Inc function [0,0,1] -> [0,1,0] all combinations\n  int tmp;\n  for(int i = 0; i < 3; i++){\n    if(check[i] == 1){\n     tmp = i;\n    }\n  }\n  check[tmp] = 0;\n  if(tmp == 2){\n    check[0] = 1;\n  }else{\n    check[tmp +1] = 1; \n  }\n}\n void dec(){// an Array Dec function [0,0,1] -> [0,1,0] all combinations\n  int tmp;\n  for(int i = 0; i < 3; i++){\n    if(check[i] == 1){\n     tmp = i;\n    }\n  }\n  check[tmp] = 0;\n  if(tmp == 0){\n    check[2] = 1;\n  }else{\n    check[tmp -1] = 1; \n  }\n}\n\nvoid shot(){ // Function that turs the motors on and of, according to the array\n   if(check[0] == 1){ // We use digitalWrite to countrol the DC Motors\n           digitalWrite(3, HIGH);  // Here we activate an 'Alcohol' Pump for example Whisky      \n           delay(4000);                 //for 4 Seconds\n           digitalWrite(3, LOW);   //Here we turn it off    \n           delay(2000); \n           \n           digitalWrite(9, HIGH);  // Than we activate a Cola. This Motor we have to activate whole the time. (It actually depends on what you want to do)\n           delay(6000);                 \n           digitalWrite(9, LOW);        \n           delay(2000);\n      }else if(check[1] == 1){\n          digitalWrite(5, HIGH);     \n           delay(4000);                 \n           digitalWrite(5, LOW);     \n           delay(2000);\n           \n           digitalWrite(9, HIGH);      \n           delay(6000);                 \n           digitalWrite(9, LOW);       \n           delay(2000);\n      }else if(check[2] == 1){\n          digitalWrite(6, HIGH);      \n           delay(4000);                 \n           digitalWrite(6, LOW);        \n           delay(2000);\n           \n           digitalWrite(9, HIGH);      \n           delay(6000);                 \n           digitalWrite(9, LOW);        \n           delay(2000);\n      }\n}</pre>",
                //bild:"shots/"
            },
            {
                text:"Here is the Final Video",
                video:"shots/Final/video.mp4"
            },
            {
                text:"Here are the links to download 2D Model of the Case and 3D Modell of the Cup."
            },
            {
                text:"<a href='downloads/Case.3dmbak' download='downloads/Case.3dmbak'>Case</a>"
            },
            {
                text:"<a href='downloads/CupModel.f3d' download='downloads/CupModel.f3d'>CupModel</a>"
            },
            {
                text:"I have choosen Attribution-NonCommercial 4.0 International (CC BY-NC 4.0), because it fun Project, that everyone schould rebuild and modify it but without any commercial purposes."
            },
            {
                text:"I have made a Modified Version of Satschakit before which i have used in this Project, also gained Information about operating an LCD 16x2 and SerialConnection."
            },
            {
                text:"My Project should be able to mix coctails, where the user can easily modify them.\nFor now it is able to run 4 Motors, but it is very easy to increase the amount of pumps, so that the user will be able to use a big spectrum of coctails.\n The whole Price of the Project it expandible it depends on how much pumps the user want to use. The avarage price of the pumps is between 10-15 Euros. For my Project I have invested more then 50 Euros."
            },
            {
                text:"While Making the Project we have to deal with Laser Cutting, 3-D Printing, PCB - Desing and Soldering, Progamming of Output and Input devices, some knowlige in physics. And one important criteria is enthusiasm."
            },
            {
                text:"==========",
                hrefs:"<a rel='license' href='http://creativecommons.org/licenses/by-nc/4.0/'><img alt='Creative Commons License' style='border-width:0' src='https://i.creativecommons.org/l/by-nc/4.0/88x31.png' /></a><br />This work is licensed under a <a rel='license' href='http://creativecommons.org/licenses/by-nc/4.0/'>Creative Commons Attribution-NonCommercial 4.0 International License</a>."
            },

        ]
    },
   

]