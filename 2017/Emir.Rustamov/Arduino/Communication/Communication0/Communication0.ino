#include <LiquidCrystal.h>
#include <SoftwareSerial.h>
SoftwareSerial mySerial(0,1);

// This is the PCB Satshakit Code with LCD and Arduino-Communication
LiquidCrystal lcd(12, 11, 10, 5, 4, 3, 2); //We taled about a lcd last Lesson
void setup() {
  mySerial.begin(9600);         // Declare Baudrate
  lcd.begin(16,2);              // columns, rows.  use 16,2 for a 16x2 LCD, etc.
  lcd.clear();                  // start with a blank screen
}

void loop() {

  if(mySerial.available()>0){
     lcd.setCursor(0,0);           // set cursor to column 0, row 0 (the first row)
     lcd.print(mySerial.readString());  //First ROW ist the Changing Message form the Arduino
     lcd.setCursor(0,1);           // set cursor to column 0, row 1
     lcd.print("From Arduino UNO"); //Second Row is the Constant ROW, not from Arduino
  }
}
