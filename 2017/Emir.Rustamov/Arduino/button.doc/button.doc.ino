const int buttonPin = 5;     // the number of the pushbutton pin
const int ledPin =  13;  
const int ledPin2 =  6;  
int buttonState = 0;
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(ledPin, OUTPUT);
  pinMode(ledPin2, OUTPUT);
  pinMode(buttonPin, INPUT);
}
void loop() {
  buttonState = digitalRead(buttonPin);
  if((buttonState == HIGH)){
    digitalWrite(ledPin, HIGH);
    digitalWrite(ledPin2, HIGH);// turn the LED on (HIGH is the voltage level)
    delay(1000);                       // wait for a second
    digitalWrite(ledPin, LOW);  
    digitalWrite(ledPin2, LOW);// turn the LED off by making the voltage LOW
    delay(1000); 
  } else {
    // turn LED off:
    digitalWrite(ledPin, HIGH);
   // digitalWrite(ledPin2, HIGH);// turn the LED on (HIGH is the voltage level)
    delay(1000);                       // wait for a second
    digitalWrite(ledPin, LOW);  
   // digitalWrite(ledPin2, LOW);// turn the LED off by making the voltage LOW
    delay(1000);
  }
}
