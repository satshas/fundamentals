int potPin = 2;    // select the input pin for the potentiometer
int ledPin = 13;
int ledPin2 = 12;
int ledPin3 = 11;
int ledPin4 = 10;
int ledPin5 = 9;
int ledPin6 = 8;// select the pin for the LED
int val = 0;       // variable to store the value coming from the sensor

void setup() {
   Serial.begin(9600);
  pinMode(ledPin, OUTPUT);
  pinMode(ledPin2, OUTPUT);
  pinMode(ledPin3, OUTPUT);
  pinMode(ledPin4, OUTPUT);
  pinMode(ledPin5, OUTPUT);
  pinMode(ledPin6, OUTPUT);
  // declare the ledPin as an OUTPUT
}

void loop() {
  val = analogRead(potPin); 
   Serial.println(val);// read the value from the sensor
   if(val < 171){
    digitalWrite(ledPin2, LOW);
    digitalWrite(ledPin3, LOW);
    digitalWrite(ledPin4, LOW);
    digitalWrite(ledPin5, LOW);
    digitalWrite(ledPin6, LOW);
    
    digitalWrite(ledPin, HIGH);  // turn the ledPin on
    delay(val);          // stop the program for some time
    digitalWrite(ledPin, LOW);
    delay(val);   
    }

    if(val > 171 && val < 341){
    digitalWrite(ledPin, LOW);
    digitalWrite(ledPin3, LOW);
    digitalWrite(ledPin4, LOW);
    digitalWrite(ledPin5, LOW);
    digitalWrite(ledPin6, LOW);

      
    digitalWrite(ledPin2, HIGH);  // turn the ledPin on
    delay(val);          // stop the program for some time
    digitalWrite(ledPin2, LOW);
    delay(val);   
    }

    if(val > 341 && val < 511){
    digitalWrite(ledPin, LOW);
    digitalWrite(ledPin2, LOW);
    digitalWrite(ledPin4, LOW);
    digitalWrite(ledPin5, LOW);
    digitalWrite(ledPin6, LOW);
      
    digitalWrite(ledPin3, HIGH);  // turn the ledPin on
    delay(val);          // stop the program for some time
    digitalWrite(ledPin3, LOW);
    delay(val);   
    }

    if(val > 511 && val < 681){
    digitalWrite(ledPin, LOW);
    digitalWrite(ledPin2, LOW);
    digitalWrite(ledPin3, LOW);
    digitalWrite(ledPin5, LOW);
    digitalWrite(ledPin6, LOW);
      
    digitalWrite(ledPin4, HIGH);  // turn the ledPin on
    delay(val);          // stop the program for some time
    digitalWrite(ledPin4, LOW);
    delay(val);   
    }

    if(val > 681 && val < 851){
    digitalWrite(ledPin, LOW);
    digitalWrite(ledPin2, LOW);
    digitalWrite(ledPin3, LOW);
    digitalWrite(ledPin4, LOW);
    digitalWrite(ledPin6, LOW);
      
    digitalWrite(ledPin5, HIGH);  // turn the ledPin on
    delay(val);          // stop the program for some time
    digitalWrite(ledPin5, LOW);
    delay(val);   
    }

    if(val > 851 && val < 1204){
    digitalWrite(ledPin, LOW);
    digitalWrite(ledPin2, LOW);
    digitalWrite(ledPin3, LOW);
    digitalWrite(ledPin4, LOW);
    digitalWrite(ledPin5, LOW);
      
    digitalWrite(ledPin6, HIGH);  // turn the ledPin on
    delay(val);          // stop the program for some time
    digitalWrite(ledPin6, LOW);
    delay(val);   
    }
   // turn the ledPin off
                    // stop the program for some time
}
