#include <SoftwareSerial.h>
SoftwareSerial mySerial(A0,A1); //Declare mySerial at ports 0,1 RX and TX


int check[3] = {0, 0, 1}; // an array of status, which of 3 motors should be activated.(I have implemented Only 3 Motors)

int back = 11; // A left Button Pin Number
int give = 12; // A buddle Butten Pin Number
int forw = 13; // A right Button Pin Number

void setup() {
  Serial.begin(9600); // Serial to debug purposes
  //3 5 6 9
  pinMode(3, OUTPUT); // Declare The Output for the Motors(Pumps)
  pinMode(5, OUTPUT); 
  pinMode(6, OUTPUT); 
  pinMode(9, OUTPUT); 
  mySerial.begin(9600); // Serial to send a message to PCB at 9600 baudrate.
  pinMode(back, INPUT);
  pinMode(give, INPUT);
  pinMode(forw, INPUT);
  //A1,A2,A3 Buttons
  
}

void loop() {


  if(digitalRead(back)){ // If the Left Button is pressed
    dec(); // an Array Dec function [0,0,1] -> [0,1,0] all combinations
    Serial.println(check[0]); // 
    Serial.println(check[1]);
    Serial.println(check[2]);
    delay(1000);
    if(check[0] == 1){
      mySerial.print("  Whisky-Lemon! ");// Send to Serial this message.
    }else if(check[1] == 1){
      mySerial.print("  Vodka-Lemon!  ");// Send to Serial this message.
    }else if(check[2] == 1){
      mySerial.print("   Gin-Lemon!   ");// Send to Serial this message.
    }
  }


  
  if(digitalRead(give)){ // If the Middle Button is pressed 
    shot(); // give us a shot
    delay(1000);
  }
  if(digitalRead(forw)){// If the Right Button is pressed
    inc();// an Array Inc function [0,0,1] -> [0,1,0] all combinations
    Serial.println(check[0]);
    Serial.println(check[1]);
    Serial.println(check[2]);
    if(check[0] == 1){
      mySerial.print("  Whisky-Lemon! ");// Send to Serial this message.
    }else if(check[1] == 1){
      mySerial.print("  Vodka-Lemon!  ");// Send to Serial this message.
    }else if(check[2] == 1){
      mySerial.print("   Gin-Lemon!   ");// Send to Serial this message.
    }
    delay(1000);
  }
}



void inc(){// an Array Inc function [0,0,1] -> [0,1,0] all combinations
  int tmp;
  for(int i = 0; i < 3; i++){
    if(check[i] == 1){
     tmp = i;
    }
  }
  check[tmp] = 0;
  if(tmp == 2){
    check[0] = 1;
  }else{
    check[tmp +1] = 1; 
  }
}
 void dec(){// an Array Dec function [0,0,1] -> [0,1,0] all combinations
  int tmp;
  for(int i = 0; i < 3; i++){
    if(check[i] == 1){
     tmp = i;
    }
  }
  check[tmp] = 0;
  if(tmp == 0){
    check[2] = 1;
  }else{
    check[tmp -1] = 1; 
  }
}

void shot(){ // Function that turs the motors on and of, according to the array
   if(check[0] == 1){ // We use digitalWrite to countrol the DC Motors
           digitalWrite(3, HIGH);  // Here we activate an 'Alcohol' Pump for example Whisky      
           delay(4000);                 //for 4 Seconds
           digitalWrite(3, LOW);   //Here we turn it off    
           delay(2000); 
           
           digitalWrite(9, HIGH);  // Than we activate a Cola. This Motor we have to activate whole the time. (It actually depends on what you want to do)
           delay(6000);                 
           digitalWrite(9, LOW);        
           delay(2000);
      }else if(check[1] == 1){
          digitalWrite(5, HIGH);     
           delay(4000);                 
           digitalWrite(5, LOW);     
           delay(2000);
           
           digitalWrite(9, HIGH);      
           delay(6000);                 
           digitalWrite(9, LOW);       
           delay(2000);
      }else if(check[2] == 1){
          digitalWrite(6, HIGH);      
           delay(4000);                 
           digitalWrite(6, LOW);        
           delay(2000);
           
           digitalWrite(9, HIGH);      
           delay(6000);                 
           digitalWrite(9, LOW);        
           delay(2000);
      }
}
