#include <SoftwareSerial.h>

int rxPin = 1;
int txPin = 4;

int x = 0;


int ledPin;
int buttonPin;

//set up new serial object
SoftwareSerial mySerial (rxPin, txPin);


boolean light = false;


void setup() {

  mySerial.begin(4800);
  mySerial.println("Welcome to Aperture Science!");
  mySerial.println("----------------------------");

  ledPin = 2;

 
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {

while(x < 5){
  if(light == true){
     mySerial.print(x);
     mySerial.println(". Hello World");
     delay(1000);
     mySerial.println("Turning off");
     mySerial.println("----------------------------");
     digitalWrite(LED_BUILTIN,LOW);
     light = false;
  }else{
    if(x == 4 ){
      delay(1000);
      mySerial.println("The cake is a lie!");
      delay(1000);
      digitalWrite(LED_BUILTIN,HIGH);
      x++;
    }else{
      delay(1000);
      mySerial.println("Turning on");
      delay(1000);
      digitalWrite(LED_BUILTIN,HIGH);
      light = true;
      x++;
        }
      }
    }
  }
