int x; 
#define BAUD (4800)


void setup() 
{
  Serial.begin(BAUD);
  pinMode(5,OUTPUT); // Step
  pinMode(4,OUTPUT); // Dir
}

void loop() 
{
  digitalWrite(4,HIGH);
  for(x = 0; x < 100; x++) 
  {
    digitalWrite(5,HIGH); 
    delay(10); 
    digitalWrite(5,LOW); 
    delay(100);
  }
  Serial.println("Pause");
  delay(1000); 
}
