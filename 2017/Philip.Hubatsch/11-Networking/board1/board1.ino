//Arduino

#include <SoftwareSerial.h>  

SoftwareSerial mySerial(10,11);  //rx & tx

void setup() {
  
  Serial.begin(9600); 
  
  mySerial.begin(9600); 

  Serial.write("Starting");
  
}

void loop() {
 
  mySerial.println("Hi from 1"); 
  
  delay(3000);
  
  if (mySerial.available() > 0) {  
    
     Serial.write(mySerial.read());  
    
   } 
}
