
#include <dht.h>                                    //shows that an outside library was added to sketch
#define SENSOR_PIN 7                                //replaces any constant called 'SENSOR_PIN' with pin '7'
int PhotoResPIN;  

dht humiditySensor;                                 //declares the variable 'dht humiditySensor'


void setup(void) {
Serial.begin(115200);                              //set the baudrate for the communication to 115200 bits per second

PhotoResPIN = A0;                                  //assigns analog pin 0 to the variable 

pinMode (PhotoResPIN, INPUT);                      //setting variable, representing the analog pin, as input

}

void loop(void) {

humiditySensor.read11(SENSOR_PIN);                 //executes not-inbuilt function to read Signal on SENSOR_PIN
  
int light = analogRead(PhotoResPIN);               //declaring the variable 'light' to the pin readings
int light1 = light / 9.8;                          //declaring the variable 'light1' to the result of the pin readings / 9.8

Serial.print("Humidity: ");                        //prints 'Humidity' as text
Serial.println(humiditySensor.humidity, 2);        //prints the corresponding humidity value in binary to a new line
Serial.print("Temperature: ");                     //prints 'Temperature' as text
Serial.println(humiditySensor.temperature, 2);     //prints the corresponding temperature value in binary to a new line
Serial.print("Lightintensity: ");                  //give out the text in quote chars in the serial monitor
Serial.println(light1);                            //give out the variable 'light1' in a new line
delay(2000);                                       //pause for 2 second before executing loop() function again
}




