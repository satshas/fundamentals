#include "U8glib.h"
#include <dht.h>
#define SENSOR_PIN 7
int PhotoResPIN;  

dht humiditySensor;

U8GLIB_ST7920_128X64 u8g(8, 9, 10, U8G_PIN_NONE);


void setup(void) {
Serial.begin(115200);

Serial.println ("Test");

PhotoResPIN = A0;

pinMode (PhotoResPIN, INPUT);

// assign default color value
if ( u8g.getMode() == U8G_MODE_R3G3B2 )
u8g.setColorIndex(255);                                       // white
else if ( u8g.getMode() == U8G_MODE_GRAY2BIT )
u8g.setColorIndex(3);                                         // max intensity
else if ( u8g.getMode() == U8G_MODE_BW )
u8g.setColorIndex(1);                                         // pixel on

}

void loop(void) {

 humiditySensor.read11(SENSOR_PIN);
  
int light = analogRead(PhotoResPIN);
int light1 = light / 9.8;

Serial.print("Humidity: ");
Serial.println(humiditySensor.humidity, 2);
Serial.print("Temperature: ");
Serial.println(humiditySensor.temperature, 2);
Serial.print("Lightintensity: ");
Serial.println(light1);
delay(1000);

// picture loop
u8g.firstPage();
do {
draw();
} while( u8g.nextPage() );

// rebuild the picture after some delay
delay(1000);
}

void draw(void) {
// graphic commands to redraw the complete screen should be placed here
u8g.setFont(u8g_font_helvB08);                                            //set font for display         
//u8g.setFont(u8g_font_osb21);

int light = analogRead(PhotoResPIN);                                     
int light1 = (light - 3 ) / 9.8;

char bufH[9];                                                             //declaring character variable bufH, pointer 9
char bufT[9];                                                             //declaring character variable bufT
char bufL[9];                                                             //declaring character variable bufL
sprintf (bufL, "%d", (int)light1);
sprintf (bufH, "%d", (int)humiditySensor.humidity);
sprintf (bufT, "%d", (int)humiditySensor.temperature);
u8g.drawStr( 0, 17, bufH);                                                //giving out variable bufH at position X Y
u8g.drawStr( 0, 8, "Humidity:");                                          //giving out text 'Humidity:' at position X Y
u8g.drawStr( 16, 17, "%");                                                //giving out text '%' at position X Y
u8g.drawStr( 0, 28, "Temperature:");                                      //giving out text 'Temperature:' at position X Y
u8g.drawStr( 16, 37, "°C");                                               //giving out text '°C' at position X Y
u8g.drawStr( 0, 37, bufT);                                                //giving out variable bufT at position X Y
u8g.drawStr( 0, 48, "Lightintensity:");                                   //giving out text 'Lightintensity:' at position X Y
u8g.drawStr( 16, 57, "%");                                                //giving out text '%' at position X Y
u8g.drawStr( 0, 57, bufL);                                                //giving out variable bufL at position X Y
}
