#include "U8glib.h"
#include <dht.h>
#define SENSOR_PIN 7
int PhotoResPIN;  

dht humiditySensor;

U8GLIB_ST7920_128X64 u8g(8, 9, 10, U8G_PIN_NONE);


void setup(void) {
Serial.begin(115200);

Serial.println ("Test");

PhotoResPIN = A0;

pinMode (PhotoResPIN, INPUT);

// assign default color value
if ( u8g.getMode() == U8G_MODE_R3G3B2 )
u8g.setColorIndex(255); // white
else if ( u8g.getMode() == U8G_MODE_GRAY2BIT )
u8g.setColorIndex(3); // max intensity
else if ( u8g.getMode() == U8G_MODE_BW )
u8g.setColorIndex(1); // pixel on

}

void loop(void) {

 humiditySensor.read11(SENSOR_PIN);
  
int light = analogRead(PhotoResPIN);
int light1 = light / 9.8;

Serial.print("Humidity: ");
Serial.println(humiditySensor.humidity, 2);
Serial.print("Temperature: ");
Serial.println(humiditySensor.temperature, 2);
Serial.print("Lightintensity: ");
Serial.println(light1);
delay(1000);

// picture loop
u8g.firstPage();
do {
draw();
} while( u8g.nextPage() );

delay(1000);                                                  //every 1 second the picture of the screen is rebuild with new sensor values
}

void draw(void) {

u8g.setFont(u8g_font_helvB08);                                //sets the font for the display output

int light = analogRead(PhotoResPIN);
int light1 = (light - 3 ) / 9.8;

char bufH[9];
char bufT[9];
char bufL[9];
sprintf (bufL, "%d", (int)light1);
sprintf (bufH, "%d", (int)humiditySensor.humidity);
sprintf (bufT, "%d", (int)humiditySensor.temperature);
u8g.drawStr( 0, 17, bufH);
u8g.drawStr( 0, 8, "Humidity:");
u8g.drawStr( 16, 17, "%");
u8g.drawStr( 0, 28, "Temperature:");
u8g.drawStr( 16, 37, "°C");
u8g.drawStr( 0, 37, bufT);
u8g.drawStr( 0, 48, "Lightintensity:");
u8g.drawStr( 16, 57, "%"); 
u8g.drawStr( 0, 57, bufL);
}
