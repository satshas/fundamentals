int ledPinGreen;    //declaring the variable as integer variable

void setup() {

ledPinGreen = 13;   //assigning pin 13 to it

Serial.begin(9600); //sets the baudrate for communication to 9600 bits per second

pinMode(ledPinGreen, OUTPUT); //setting variabla, representing pin 13, as output pin

}

void loop() {

digitalWrite(ledPinGreen, LOW);    //sets the output of pin 13 to low (0V)
delay(500);               //let the programm stop for 500 microseconds
digitalWrite(ledPinGreen, HIGH);   //sets the output of pin 13 to high (5V)
delay(500);               //let the programm stop for 500 microseconds

}
