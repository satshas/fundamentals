int ledPinGreen;
int ledPinBlue;
int ButPin;

void setup() {

Serial.begin(9600);

ledPinGreen = 13;
ledPinBlue = 6;
ButPin = 5;

pinMode(ledPinBlue, OUTPUT); 
pinMode(ledPinGreen, OUTPUT);
pinMode(ButPin, INPUT);

}

void loop() {

if(digitalRead(ButPin) == HIGH) {

for(int x=0; x < 5; x++) {

digitalWrite(ledPinBlue, HIGH);
delay(500);
digitalWrite(ledPinBlue, LOW);
delay(500);
}
  
}

else{

digitalWrite(ledPinGreen, LOW);
delay(500);
digitalWrite(ledPinGreen, HIGH);
delay(500);
}
}